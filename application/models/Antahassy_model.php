<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Antahassy_model extends CI_Model{

     public function __construct(){
          parent::__construct();
          date_default_timezone_set('Asia/Jakarta');
     }

     public function found_username($username){ 
          $this->db->select('user_name');
          $query = $this->db->get_where('arsip_user', array('user_name' => $username));
          if($query->num_rows() == 0){
               return true;
          }else{
               return false;
          }
     }

     public function login_user($username, $password){
          $query = $this->db->get_where('arsip_user', array(
               'user_name'    => $username,
               'password'     => $password
          ));
          if($query->num_rows() > 0){
               return $query->row();
          }else{
               return false;
          }
     }
}