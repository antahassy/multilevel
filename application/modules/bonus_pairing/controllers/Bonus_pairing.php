<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bonus_pairing extends CI_Controller {

	public function __construct(){
		parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Bonus_pairing_model','model');
		if (! $this->ion_auth->logged_in()){
            redirect('auth', 'refresh');
        }
	}

    public $id_menu = '6';

	public function index(){
        $id_group = $this->db->get_where('users_groups', array(
            'user_id' => $this->ion_auth->user()->row()->id
        ))->row()->group_id;
        $akses = $this->db->get_where('rel_group', array(
            'id_group'  => $id_group,
            'id_menu'   => $this->id_menu
        ))->result();

        if($akses){
            $akses_temp = array();
            foreach ($akses as $key) {
                array_push($akses_temp, $key->akses);
            }
            if (in_array('3', $akses_temp)){
                $data['akses_temp'] = $akses_temp;
                $data['title'] = 'Bonus Pairing';
                $data['id_group'] = $id_group;
                $this->load->view('header', $data);
                $this->load->view('index');
                $this->load->view('footer');
            }else{
                echo "Akses Ditolak";
            }
        }else{
            echo "Akses Ditolak";
        }
	}

	public function s_side_data(){
        $id_user = $this->ion_auth->user()->row()->id;
        $id_group = $this->db->get_where('users_groups', array(
            'user_id' => $id_user
        ))->row()->group_id;
        $akses = $this->db->get_where('rel_group', array(
            'id_group'  => $id_group,
            'id_menu'   => $this->id_menu
        ))->result();

        if($akses){
            $akses_temp = array();
            foreach ($akses as $key) {
                array_push($akses_temp, $key->akses);
            }
        }

        $month_format = array (1 => 
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $list = $this->model->s_side_data($id_user, $id_group);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->username;

            $s_tanggal = explode('-', $field->tanggal);
            $row[] = $s_tanggal[2] .'/'. $month_format[(int)$s_tanggal[1]] .'/'. $s_tanggal[0];

            
            $left = $this->model->left_username($field->id_kiri);
            if($left){
                $row[] = $left->username . '<br>' . number_format($field->kiri , 0, ',', '.');
            }else{
                $row[] = '' . '<br>' . number_format($field->kiri , 0, ',', '.');
            }
            
            $right = $this->model->right_username($field->id_kanan);
            if($right){
                $row[] = $right->username . '<br>' . number_format($field->kanan , 0, ',', '.');
            }else{
                $row[] = '' . '<br>' . number_format($field->kanan , 0, ',', '.');
            }

            $row[] = number_format($field->pairing , 0, ',', '.');
            $row[] = number_format($field->bonus , 0, ',', '.');

            // if (in_array('2', $akses_temp) && in_array('4', $akses_temp)){
            //     $row[] = '<button data="' . $field->id_paket . '" class="btn btn-sm btn-rounded btn-outline-warning" id="btn_edit" title="Edit" style="margin: 2.5px;">Edit</button> <button data="' . $field->id_paket . '" class="btn btn-sm btn-rounded btn-outline-danger" id="btn_delete" title="Hapus" style="margin: 2.5px;">Hapus</button>';
            // }
            // if (! in_array('2', $akses_temp) && in_array('4', $akses_temp)){
            //     $row[] = '<button data="' . $field->id_paket . '" class="btn btn-sm btn-rounded btn-outline-danger" id="btn_delete" title="Hapus" style="margin: 2.5px;">Hapus</button>';
            // }
            // if (in_array('2', $akses_temp) && ! in_array('4', $akses_temp)){
            //     $row[] = '<button data="' . $field->id_paket . '" class="btn btn-sm btn-rounded btn-outline-warning" id="btn_edit" title="Edit" style="margin: 2.5px;">Edit</button>';
            // }
            // if (! in_array('2', $akses_temp) && ! in_array('4', $akses_temp)){
            //     $row[] = '';
            // }

            if($field->created_at != ''){
                $s_created = explode(' ', $field->created_at);
                $s_created_date = explode('-', $s_created[0]);
                $row[] = $s_created_date[2] .'/'. $month_format[(int)$s_created_date[1]] .'/'. $s_created_date[0] . '<br>' . $s_created[1];
            }else{
                $row[] = '';
            }

            if($field->updated_at != ''){
                $s_updated = explode(' ', $field->updated_at);
                $s_updated_date = explode('-', $s_updated[0]);
                $row[] = $s_updated_date[2] .'/'. $month_format[(int)$s_updated_date[1]] .'/'. $s_updated_date[0] . '<br>' . $s_updated[1];
            }else{
                $row[] = '';
            }
 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all($id_user, $id_group),
            "recordsFiltered" => $this->model->count_filtered($id_user, $id_group),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function data_grafik(){
        $id_user = $this->input->post('id_user');
        $periode = $this->input->post('periode');

        $pairing_data = array();
        $pairing_param = array();
        $data = $this->model->pairing_user($id_user, $periode);
        if($data){
            for($i = 0; $i < count($data); $i ++){
                array_push($pairing_param, $data[$i]->parameter);
                $log_jml = $this->model->all_pairing($data[$i]->parameter, $periode, $id_user);
                array_push($pairing_data, array(
                    'parameter'     => $data[$i]->parameter,
                    'data'          => (int)$log_jml->bonus
                ));
            }
            if(count($pairing_param) == 1){
                if($periode == '1'){
                    array_unshift($pairing_data, array(
                        'parameter'     => date('Y-m-d', strtotime($pairing_param[0] . ' + 1 day')),
                        'data'          => 0,
                    ));
                    array_unshift($pairing_param, date('Y-m-d', strtotime($pairing_param[0] . ' + 1 day')));
                }
                if($periode == '2'){
                    array_unshift($pairing_data, array(
                        'parameter'     => date('Y-m', strtotime($pairing_param[0] . ' + 1 month')),
                        'data'          => 0,
                    ));
                    array_unshift($pairing_param, date('Y-m', strtotime($pairing_param[0] . ' + 1 month')));
                }
                if($periode == '3'){
                    array_unshift($pairing_data, array(
                        'parameter'     => date('Y', strtotime($pairing_param[0] . ' + 1 year')),
                        'data'          => 0,
                    ));
                    array_unshift($pairing_param, date('Y', strtotime($pairing_param[0] . ' + 1 year')));
                }
            }
        }else{
            if($periode == '1'){
                array_unshift($pairing_data, array(
                    'parameter'     => date('Y-m-d', strtotime(date('Y-m-d') . ' + 1 day')),
                    'data'          => 0,
                ));
                array_unshift($pairing_data, array(
                    'parameter'     => date('Y-m-d'),
                    'data'          => 0,
                ));

                array_push($pairing_param, date('Y-m-d', strtotime(date('Y-m-d') . ' + 1 day')));
                array_push($pairing_param, date('Y-m-d'));
            }
            if($periode == '2'){
                array_unshift($pairing_data, array(
                    'parameter'     => date('Y-m', strtotime(date('Y-m') . ' + 1 month')),
                    'data'          => 0,
                ));
                array_unshift($pairing_data, array(
                    'parameter'     => date('Y-m'),
                    'data'          => 0,
                ));

                array_push($pairing_param, date('Y-m', strtotime(date('Y-m') . ' + 1 month')));
                array_push($pairing_param, date('Y-m'));
            }
            if($periode == '3'){
                array_unshift($pairing_data, array(
                    'parameter'     => date('Y', strtotime(date('Y') . ' + 1 year')),
                    'data'          => 0,
                ));
                array_unshift($pairing_data, array(
                    'parameter'     => date('Y'),
                    'data'          => 0,
                ));

                array_push($pairing_param, date('Y', strtotime(date('Y') . ' + 1 year')));
                array_push($pairing_param, date('Y'));
            }
        }

        $object = array(
            'pairing_data'      => $pairing_data,
            'pairing_param'     => $pairing_param
        );

        //////////////////////////////////////////////

        echo json_encode($object);
    }

    public function autocomplete_username(){
        if(isset($_GET['term'])){
            $result = $this->model->autocomplete_username($_GET['term']); 
            if(count($result) > 0){
                foreach ($result as $row){
                    $all_result[] = $row->username;
                }
            echo json_encode($all_result);
            }
        }
    }

    public function username_complete(){
        $username = $this->input->post('username');
        $data = $this->model->username_complete($username);
        echo json_encode($data);
    }
}
