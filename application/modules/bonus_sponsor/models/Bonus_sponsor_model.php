<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bonus_sponsor_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	// Datatable server side processing
    public function s_side_datatables_query($id_user, $id_group){
        $this->db->select('
            tb_bonus_sponsor.sponsor_id,
            tb_bonus_sponsor.tanggal,
            tb_bonus_sponsor.upline_id,
            tb_bonus_sponsor.bonus,
            tb_bonus_sponsor.nilai,
            tb_bonus_sponsor.paket,
            tb_bonus_sponsor.created_by,
            tb_bonus_sponsor.created_at,
            tb_bonus_sponsor.updated_by,
            tb_bonus_sponsor.updated_at,
            users.username
        ');
        $column_order = array(null, 
            'tb_bonus_sponsor.sponsor_id',
            'tb_bonus_sponsor.tanggal',
            'tb_bonus_sponsor.upline_id',
            'tb_bonus_sponsor.bonus',
            'tb_bonus_sponsor.nilai',
            'tb_bonus_sponsor.paket',
            'tb_bonus_sponsor.created_by',
            'tb_bonus_sponsor.created_at',
            'tb_bonus_sponsor.updated_by',
            'tb_bonus_sponsor.updated_at',
            'users.username'
        );
        $column_search = array(
            'tb_bonus_sponsor.sponsor_id',
            'tb_bonus_sponsor.tanggal',
            'tb_bonus_sponsor.upline_id',
            'tb_bonus_sponsor.bonus',
            'tb_bonus_sponsor.nilai',
            'tb_bonus_sponsor.paket',
            'tb_bonus_sponsor.created_by',
            'tb_bonus_sponsor.created_at',
            'tb_bonus_sponsor.updated_by',
            'tb_bonus_sponsor.updated_at',
            'users.username'
        ); 
        if($id_group == '2'){
            $this->db->where('tb_bonus_sponsor.sponsor_id', $id_user);
        }
        $this->db->where('tb_bonus_sponsor.deleted_at','');
        $this->db->from('tb_bonus_sponsor');
        $this->db->join('users', 'tb_bonus_sponsor.member_id = users.id', 'left');
        $i = 0;
        foreach ($column_search as $item){
            if($_POST['search']['value']  != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('tb_bonus_sponsor.id_bonus_sponsor', 'desc');
        }
    }

    public function s_side_data($id_user, $id_group){
        $this->s_side_datatables_query($id_user, $id_group);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function count_filtered($id_user, $id_group){
        $this->s_side_datatables_query($id_user, $id_group);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all($id_user, $id_group){
        if($id_group == '2'){
            $this->db->where('tb_bonus_sponsor.sponsor_id', $id_user);
        }
        $this->db->where('tb_bonus_sponsor.deleted_at','');
        $this->db->from('tb_bonus_sponsor');
        return $this->db->count_all_results();
    }
    // End datatable server side processing

    public function sponsor_username($id_sponsor){
        $this->db->select('username');
        $query = $this->db->get_where('users', array('id' => $id_sponsor));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function upline_username($id_upline){
        $this->db->select('username');
        $query = $this->db->get_where('users', array('id' => $id_upline));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function sponsor_user($id_user, $periode){
        $this->db->order_by('tanggal','asc');
        if($periode == '1'){
            $this->db->select('tanggal as parameter');
        }
        if($periode == '2'){
            $this->db->select('MID(tanggal,1,7) as parameter');
        }
        if($periode == '3'){
            $this->db->select('MID(tanggal,1,4) as parameter');
        }
        $this->db->distinct();
        $query = $this->db->get_where('tb_bonus_sponsor', array(
            'sponsor_id' => $id_user,
            'deleted_at' => ''
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function all_sponsor($parameter, $periode, $id_user){
        $this->db->select_sum('bonus');
        $this->db->where('sponsor_id', $id_user);
        if($periode == '1'){
            $this->db->where('tanggal', $parameter);
        }
        if($periode == '2'){
            $this->db->where('MID(tanggal,1,7)', $parameter);
        }
        if($periode == '3'){
            $this->db->where('MID(tanggal,1,4)', $parameter);
        }
        $this->db->where('deleted_at','');
        $this->db->from('tb_bonus_sponsor');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function autocomplete_username($data){
        $this->db->order_by('username','asc');
        $this->db->select('username');
        $this->db->like('username', $data, 'both');
        return $this->db->get('users')->result();
    }

    public function username_complete($username){
        $this->db->select('id');
        $query = $this->db->get_where('users', array(
            'username' => $username
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }
}



