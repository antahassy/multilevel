<style>
    .thead-dark tr th{
        color: #fff !important;
    }
    label{
        margin-top: 5px;
        font-weight: 600 !important;
    }
</style>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <?php if (in_array('1', $akses_temp)){ ?>
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5"><?php echo $title ?></h5>
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                    <button class="btn btn-light-success font-weight-bolder btn-sm" id="btn_add">Tambah Data</button>
                <?php }else{ ?>
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5"><?php echo $title ?></h5>
                <?php } ?>
            </div>
            <div class="d-flex align-items-center">
            </div>
        </div>
    </div>
    <div class="d-flex flex-column-fluid">
        <div class="container">
            <div class="row" style="min-height: 68vh;">
                <div class="col-lg-12 col-xxl-4 order-1 order-xxl-2">
                    <div class="card card-custom card-stretch gutter-b">
                        <div class="card-header border-0">
                            <h3 class="card-title font-weight-bolder text-dark">Daftar <?php echo $title ?></h3>
                        </div>
                        <div class="card-body pt-0">
                            <table id="table_data" class="table table-striped table-vcenter" style="width: 100%;">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>Sponsor</th>
                                        <th>Bonus Sponsor</th>
                                        <th>Upline</th>
                                        <th>Member</th>
                                        <th>Paket Member</th>
                                        <th>Nilai Paket</th>
                                        <th>Dibuat</th>
                                        <th>Diupdate</th>
                                    </tr>
                                </thead>
                                <tbody> 
                                </tbody>
                            </table>
                        </div>
                        <div class="card-header border-0">
                            <h3 class="card-title font-weight-bolder text-dark">Grafik <?php echo $title ?></h3>
                        </div>
                        <div class="card-body pt-0">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Periode</label>
                                    <select class="form-control" name="s_periode" id="s_periode">
                                        <option value="1">Harian</option>
                                        <option value="2">Bulanan</option>
                                        <option value="3">Tahunan</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <?php if($id_group != 1){ ?>
                                        <div style="display: none">
                                            <label>Member</label>
                                            <input type="text" name="s_member" id="s_member" class="form-control" placeholder="Autocomplete" value="<?php echo $this->ion_auth->user()->row()->username?>" readonly="">
                                            <input type="hidden" name="s_member_hidden" id="s_member_hidden" value="<?php echo $this->ion_auth->user()->row()->id?>">
                                        </div>
                                    <?php }else{ ?>
                                        <div>
                                            <label>Member</label>
                                            <input type="text" name="s_member" id="s_member" class="form-control" placeholder="Autocomplete">
                                            <input type="hidden" name="s_member_hidden" id="s_member_hidden">
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="col-md-12" style="margin-top: 25px;">
                                    <div class="canvas_content">
                                        <div id="canvas_area_all">
                                            <canvas id="line_chart_all" style="position: relative;"></canvas>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo site_url('assets/project/js/modul_bonus_sponsor.js?t=').mt_rand()?>"></script>
