<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('Dashboard_model', 'model');
        if (! $this->ion_auth->logged_in()){
            redirect('auth', 'refresh');
        }
        date_default_timezone_set('Asia/Jakarta');
    }  

    public $id_menu = '1';

    public function index(){
        $month_format = array (1 => 
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $s_login = explode(' ', $this->ion_auth->user()->row()->logged_in);
        $s_login_date = explode('-', $s_login[0]);
        $data['last_login'] = $s_login_date[2] .'/'. $month_format[(int)$s_login_date[1]] .'/'. $s_login_date[0] . ' ' . $s_login[1];
        $jam=date('H');
        if($jam >= 11 && $jam < 15){
            $data['waktu'] = "Siang";
        }
        if($jam >= 15 && $jam < 18){
            $data['waktu'] = "Sore";
        }
        if($jam >= 18 && ($jam < 24 || $jam < 00)){
            $data['waktu'] = "Malam";
        }
        if(($jam >= 24 || $jam >= 00) && $jam <= 10){
            $data['waktu'] = "Pagi";
        }

        $id_group = $this->db->get_where('users_groups', array(
            'user_id' => $this->ion_auth->user()->row()->id
        ))->row()->group_id;
        $data['level'] = $this->db->get_where('groups', array(
            'id' => $id_group
        ))->row()->description;
        $akses = $this->db->get_where('rel_group', array(
            'id_group'  => $id_group,
            'id_menu'   => $this->id_menu
        ))->result();

        if($akses){
            $akses_temp = array();
            foreach ($akses as $key) {
                array_push($akses_temp, $key->akses);
            }
            if (in_array('3', $akses_temp)){
                $data['akses_temp'] = $akses_temp;
                $data['title'] = 'Dashboard';
                $this->load->view('header', $data);
                $this->load->view('index');
                $this->load->view('footer');
            }else{
                echo "Akses Ditolak";
            }
        }else{
            echo "Akses Ditolak";
        }
    }
}