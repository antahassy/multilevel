<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <?php if (in_array('1', $akses_temp)){ ?>
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5"><?php echo $title ?></h5>
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                    <button class="btn btn-light-success font-weight-bolder btn-sm" id="btn_add">Tambah Data</button>
                <?php }else{ ?>
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5"><?php echo $title ?></h5>
                <?php } ?>
            </div>
            <div class="d-flex align-items-center">
            </div>
        </div>
    </div>
    <div class="d-flex flex-column-fluid">
        <div class="container">
            <div class="row" style="min-height: 68vh;">
                <div class="col-lg-12 col-xxl-4 order-1 order-xxl-2">
                    <div class="card card-custom card-stretch gutter-b">
                        <div class="card-header border-0">
                            <h3 class="card-title font-weight-bolder text-dark">Selamat Datang</h3>
                        </div>
                        <div class="card-body pt-0">
                            <div>
                                Halo <b><?php echo $this->ion_auth->user()->row()->nama ?></b>, Selamat <?php echo $waktu ?>
                            </div>
                            <div>
                                Login terakhir anda dicatat pada <?php echo $last_login ?>
                            </div>
                            <div>
                                Level akses anda adalah sebagai <?php echo $level ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>