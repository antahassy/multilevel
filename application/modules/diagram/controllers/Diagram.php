<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diagram extends CI_Controller {

	public function __construct(){
		parent::__construct(); 
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Diagram_model','model');
		if (! $this->ion_auth->logged_in()){
            redirect('auth', 'refresh');
        }
	}

	public $id_menu = '3';

	public function index(){
        $id_group = $this->db->get_where('users_groups', array(
            'user_id' => $this->ion_auth->user()->row()->id
        ))->row()->group_id;
        $akses = $this->db->get_where('rel_group', array(
            'id_group'  => $id_group,
            'id_menu'   => $this->id_menu
        ))->result();

        if($akses){
            $akses_temp = array();
            foreach ($akses as $key) {
                array_push($akses_temp, $key->akses);
            }
            if (in_array('3', $akses_temp)){
                $data['akses_temp'] = $akses_temp;
                $data['title'] = 'Diagram';
                $this->load->view('header', $data);
                $this->load->view('index');
                $this->load->view('footer');
            }else{
                echo "Akses Ditolak";
            }
        }else{
            echo "Akses Ditolak";
        }
	}

	public function tree_process(){
		$s_level = '2';
		$by = $this->ion_auth->user()->row()->username;
		$id_sponsor = $this->ion_auth->user()->row()->id;
		$tree = $this->input->post('tree');
		
		$length = 0;
		for($i = 1; $i < count($tree); $i ++){
			$split = explode('_', $tree[$i]['paket']);
			$paket = $split[0];
			$id_upline = $tree[$i]['up'];
			$node = $tree[$i]['node'];
			$username = $tree[$i]['name']; 
			$c_account = $this->model->check_account($username);
			if(! $c_account){
				$s_account   = $this->model->save_account($by, $username); 
				if($s_account){
					$this->ion_auth->reset_password($username, $username);
					$account_id = $this->model->check_account($username);
					$id_user = $account_id->id;
					$level_group = $this->model->save_user_grup($id_user, $s_level);
					$p_detail = $this->model->paket_detail($paket);
					$bonus_sponsor = 10 * ((int)$p_detail->nilai / 100);
					$s_sponsor = $this->model->save_sponsor($id_sponsor, $by, $bonus_sponsor, $id_upline, $id_user, $p_detail->nama, $p_detail->nilai);
				}
			}else{
				$id_user = $c_account->id;
			}
			for($x = 0; $x < count($tree[$i]['down']); $x ++){
				$tree[$i]['down'][$x] = (int)$tree[$i]['down'][$x];
			}
			$downline = json_encode($tree[$i]['down']);
			$c_t_account = $this->model->check_tree_account($id_user);
			if($c_t_account){
				$u_tree = $this->model->update_tree($id_upline, $node, $downline, $id_user, $paket, $by);
				if($u_tree){
					$length = $length + 1;
				}
			}else{
				$s_tree = $this->model->save_tree($id_upline, $node, $downline, $id_user, $paket, $by);
				if($s_tree){
					$length = $length + 1;
				}
			}
			if($length == (count($tree) - 1)){
				$message['success'] = true;
				echo json_encode($message);
			}
		}
	}

	public function all_tree(){
		$id_user = $this->ion_auth->user()->row()->id;
		$id_group = $this->db->get_where('users_groups', array(
            'user_id' => $id_user
        ))->row()->group_id;
		$data = $this->model->all_tree($id_user, $id_group);
		for($i = 0; $i < count($data); $i ++){
			if($data[$i]->del != ''){
				$data[$i]->paket = '_';
			}else{
				$data[$i]->paket = $data[$i]->id_paket . '_' . $data[$i]->paket . '_' . $data[$i]->id_user;
			}
			$data[$i]->up = (int)$data[$i]->up;
			$data[$i]->id_user = (int)$data[$i]->id_user;
			$data[$i]->node = (int)$data[$i]->node;
			$data[$i]->down = json_decode($data[$i]->down);
			unset($data[$i]->del);
			unset($data[$i]->id_paket);
		}
		$uplines = array();
		foreach ($data as $row) {
			$arr_row = array(
				'id' 	=> $row->id_user,
				'up' 	=> $row->up,
				'down' 	=> $row->down
			);
			array_push($uplines, $arr_row);
		}
		$del_count = array();
		for($i = 0; $i < count($uplines); $i ++){
			if($uplines[$i]['up'] == $uplines[0]['up'] && $uplines[$i]['id'] != $uplines[0]['id']){
				array_splice($uplines, $i, 1); 
			}
		}
		$new_data = array();
		array_push($new_data, $data[0]);
		foreach($uplines as $row){
			foreach($data as $key){
				if(in_array($key->node, $row['down'])){
					array_push($new_data, $key);
				}
			}
		}
		echo json_encode($new_data);
	}

	public function s_paket(){
		$data = $this->model->s_paket();
		echo json_encode($data);
	}

	public function check_account(){
		$username = $this->input->post('username');
		$data = $this->model->check_account($username);
		if($data){
			$message['exist'] = true;
            echo json_encode($message);
		}else{
			$message['success'] = true;
            echo json_encode($message);
		}
	}

	public function check_node(){
		$node = $this->input->post('node');
		$data = $this->model->check_node($node);
		if($data){
			$message['exist'] = true;
            echo json_encode($message);
		}else{
			$message['success'] = true;
            echo json_encode($message);
		}
	}
}
