<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diagram_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

    public function all_tree($id_user, $id_group){
        $this->db->select('
            tb_member.node,
            tb_member.id_upline as up,
            tb_member.id_user,
            tb_member.downline as down,
            tb_paket.id_paket,
            tb_paket.icon as paket,
            tb_paket.deleted_at as del,
            users.username as name,
        ');
        if($id_group == '2'){
            $this->db->where('tb_member.id_user >=', $id_user);
        }
        $this->db->where('tb_member.deleted_at','');
        $this->db->from('tb_member');
        $this->db->join('tb_paket', 'tb_member.id_paket = tb_paket.id_paket', 'left');
        $this->db->join('users', 'tb_member.id_user = users.id', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }

    public function save_tree($id_upline, $node, $downline, $id_user, $paket, $by){
        $data = array(
            'id_upline'     => $id_upline,
            'id_user'       => $id_user,
            'id_paket'      => $paket,
            'node'          => $node,
            'downline'      => $downline,
            'created_by'    => $by,
            'created_at'    => date('Y-m-d H:i:s')
        );
        $query = $this->db->insert('tb_member', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function update_tree($id_upline, $node, $downline, $id_user, $paket, $by){
        $data = array(
            'id_upline'     => $id_upline,
            'id_paket'      => $paket,
            'node'          => $node,
            'downline'      => $downline,
            'updated_by'    => $by,
            'updated_at'    => date('Y-m-d H:i:s')
        );
        $this->db->where('id_user', $id_user);
        $this->db->update('tb_member', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function s_paket(){
        $this->db->select('id_paket, nama, nilai');
        $query = $this->db->get_where('tb_paket', array(
            'deleted_at'   => ''
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function check_account($username){
        $this->db->select('id');
        $query = $this->db->get_where('users', array('username' => $username));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function check_node($node){
        $this->db->select('id_member');
        $query = $this->db->get_where('tb_member', array('node' => $node));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function save_account($by, $username){
        $data = array(
            'username'      => $username,
            'nama'          => $username,
            'created_at'    => date('Y-m-d H:i:s'),
            'created_by'    => $by,
            'active'        => 1
        );
        $query = $this->db->insert('users', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function save_user_grup($id_user, $s_level){
        $data = array(
            'user_id'                   => $id_user,
            'group_id'                  => $s_level
        );
        $query = $this->db->insert('users_groups', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function save_sponsor($id_sponsor, $by, $bonus_sponsor, $id_upline, $id_user, $p_nama, $p_nilai){
        $data = array(
            'sponsor_id'    => $id_sponsor,
            'tanggal'       => date('Y-m-d'),
            'bonus'         => $bonus_sponsor,
            'upline_id'     => $id_upline,
            'member_id'     => $id_user,
            'paket'         => $p_nama,
            'nilai'         => $p_nilai,
            'created_by'    => $by,
            'created_at'    => date('Y-m-d H:i:s')
        );
        $query = $this->db->insert('tb_bonus_sponsor', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function paket_detail($paket){
        $this->db->select('nama, nilai');
        $query = $this->db->get_where('tb_paket', array(
            'id_paket'      => $paket,
            'deleted_at'    => ''
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function check_tree_account($id_user){
        $this->db->select('id_member');
        $query = $this->db->get_where('tb_member', array('id_user' => $id_user));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }
}
