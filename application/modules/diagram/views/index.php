<style type="text/css">
    .thead-dark tr th{
        color: #fff !important;
    }
    .tf-tree li{
        padding: 0 2.5px;
    }
    .tf-tree .tf-nc, .tf-tree .tf-node-content{
        padding: 2.5px 5px;
    }
    .tf-nc{
        cursor: pointer;
        font-size: 14px;
        margin: -3px 0;
    }
    .tf-nc:hover{
        background: aqua;
    }
    #btn_process, #btn_main_tree{
        margin-bottom: 10px;
    }
    .thead-dark tr th{
        color: #fff !important;
    }
    form label{
        margin-top: 5px;
        font-weight: 600 !important;
    }
</style>
<script type="text/javascript">
    window.require = function() {
        return window.treeify;
    };
    <?php if(in_array('2', $akses_temp)){ ?>
        var tree_edit = true;
    <?php }else{ ?>
        var tree_edit = false;
    <?php } ?>
</script>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <?php if (in_array('1', $akses_temp)){ ?>
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5"><?php echo $title ?></h5>
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                    <button class="btn btn-light-success font-weight-bolder btn-sm" id="btn_add">Tambah Data</button>
                <?php }else{ ?>
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5"><?php echo $title ?></h5>
                <?php } ?>
            </div>
            <div class="d-flex align-items-center">
            </div>
        </div>
    </div>
    <div class="d-flex flex-column-fluid">
        <div class="container">
            <div class="row" style="min-height: 68vh;">
                <div class="col-lg-12 col-xxl-4 order-1 order-xxl-2">
                    <div class="card card-custom card-stretch gutter-b">
                        <div class="card-body pt-0">
                            <div class="row">
                                <div class="col-md-12" style="padding: 15px;">
                                    <div class="row">
                                        <div class="col-md-12 text-center" style="padding: 10px;">
                                            <input type="text" name="node_value" id="node_value" class="form-control mb-2" placeholder="Node / Nama">
                                            <button type="button" class="btn btn-outline-primary font-weight-bolder btn-sm" id="btn_node_find">Cari</button>
                                        </div>
                                        <div class="col-md-12 text-center" style="padding: 10px;">
                                            <button type="button" class="btn btn-outline-warning font-weight-bolder btn-sm mb-1" id="btn_node_min">Min</button>
                                            <span id="node_min" style="padding-left: 15px; font-weight: 800;">-</span>
                                            <br>
                                            <button type="button" class="btn btn-outline-warning font-weight-bolder btn-sm" id="btn_node_max">Max</button>
                                            <span id="node_max" style="padding-left: 15px; font-weight: 800;">-</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="padding: 15px;">
                                    <div class="text-center" id="process_content" style="display: none;">
                                        <button class="btn btn-outline-primary font-weight-bolder btn-sm" id="btn_process">Simpan</button>
                                    </div>
                                    <div class="text-center" id="back_content" style="display: none;">
                                        <button class="btn btn-outline-success" id="btn_main_tree">Diagram Utama</button>
                                    </div>
                                    <div class="tf-tree text-center" style="overflow: visible;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal animated" id="modal_tree_data" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit</h5>
            </div>
            <div class="modal-body"> 
                <div class="col-md-12">
                    <input type="hidden" name="data_upline" id="data_upline">
                    <label>Node</label>
                    <input type="text" name="data_node" id="data_node" class="form-control">
                    <label>Username</label>
                    <input type="text" name="data_name" id="data_name" class="form-control">
                    <label>Paket</label>
                    <select class="form-control" id="data_paket" name="data_paket">
                        <option value="">Pilih Paket</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline-primary btn_modal_action" id="btn_process_tree_data">Update</button>
                <button type="button" class="btn btn-outline-primary btn_modal_action" id="btn_add_tree_data">Add</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo site_url('assets/project/js/modul_diagram.js?t=').mt_rand()?>"></script>
