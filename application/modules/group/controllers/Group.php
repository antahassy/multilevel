<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends CI_Controller {

	public function __construct(){
		parent::__construct(); 
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Group_model','model');
		if (! $this->ion_auth->logged_in() || ! $this->ion_auth->is_admin()){
            redirect('auth', 'refresh');
        }
	}

	public function index(){
        $data['title'] = 'Group';

        $this->load->view('header', $data);
        $this->load->view('index');
        $this->load->view('footer');
	}

	public function akses($id){
		$level = $this->model->group_level($id);
		$data['title'] = 'Akses ' . $level->description;
		$data['menu'] = $this->model->get_datatables_menu();

        $this->load->view('header', $data);
        $this->load->view('akses');
        $this->load->view('footer');
	}

	public function s_side_data(){
        $month_format = array (1 => 
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $list = $this->model->s_side_data();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->name;
            $row[] = $field->description;
            $row[] = '<a class="btn btn-sm btn-rounded btn-outline-primary" href="' . site_url('group/akses/' . $field->id) . '" title="Akses" style="margin: 2.5px;">Akses</a> <button data="' . $field->id . '" class="btn btn-sm btn-rounded btn-outline-warning" id="btn_edit" title="Edit" style="margin: 2.5px;">Edit</button> <button data="' . $field->id . '" class="btn btn-sm btn-rounded btn-outline-danger" id="btn_delete" title="Hapus" style="margin: 2.5px;">Hapus</button>';

            if($field->created_at != ''){
                $s_created = explode(' ', $field->created_at);
                $s_created_date = explode('-', $s_created[0]);
                $row[] = $s_created_date[2] .'/'. $month_format[(int)$s_created_date[1]] .'/'. $s_created_date[0] . '<br>' . $s_created[1];
            }else{
                $row[] = '';
            }

            if($field->updated_at != ''){
                $s_updated = explode(' ', $field->updated_at);
                $s_updated_date = explode('-', $s_updated[0]);
                $row[] = $s_updated_date[2] .'/'. $month_format[(int)$s_updated_date[1]] .'/'. $s_updated_date[0] . '<br>' . $s_updated[1];
            }else{
                $row[] = '';
            }
 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function save(){
    	$message['type'] = 'disimpan';
    	$by = $this->ion_auth->user()->row()->username;
        $data   = $this->model->save($by);
        if($data){
        	$message['success'] = true;
        	echo json_encode($message);
        }
    }

    public function edit(){
        $id = $this->input->post('id');
        $data = $this->model->edit($id); 
        echo json_encode($data);
    }

    public function update(){
    	$message['type'] = 'diupdate';
        $id = $this->input->post('id_data');
        $by = $this->ion_auth->user()->row()->username;
        $data = $this->model->update($id, $by);
        if($data){
        	$message['success'] = true;
        	echo json_encode($message);
        }
    }

    public function delete(){
        $id = $this->input->post('id');
        $by = $this->ion_auth->user()->row()->username;
        $data = $this->model->delete($id, $by); 
        if($data){
        	$d_menu = $this->model->delete_menu($id);
        	$message['success'] = true;
        	echo json_encode($message);
        }
    }

	public function ajax_akses()
	{

		//$this->_validate();
		$tambah_data = array();
		$group = $this->input->post('id_group');
		$tambah = $this->input->post('tambah');
		if(!empty($tambah)){
			$cek_tambah = $this->db->get_where('rel_group',array('id_group'=>$group,'akses'=>1));
			if($cek_tambah->num_rows() == 0)
			{
				foreach ($tambah as $row_tambah) 
				{
					$tambah_data['id_group'] = $group;
					$tambah_data['id_menu'] = $row_tambah;
					$tambah_data['akses'] = 1;
					$this->db->insert('rel_group',$tambah_data);
				}
			}
			else
			{
				$this->db->delete('rel_group',array('id_group'=>$group,'akses'=>1));
				foreach ($tambah as $row_tambah) 
				{
					$tambah_data['id_group'] = $group;
					$tambah_data['id_menu'] = $row_tambah;
					$tambah_data['akses'] = 1;
					$this->db->insert('rel_group',$tambah_data);
				}
			}
		}
		else
		{
			$this->db->delete('rel_group',array('id_group'=>$group,'akses'=>1));
		}

		$edit = $this->input->post('edit');
		if(!empty($edit)){
			$edit_data = array();
			$cek_edit = $this->db->get_where('rel_group',array('id_group'=>$group,'akses'=>2));
			if($cek_edit->num_rows() == 0)
			{
				foreach ($edit as $row_edit) 
				{
					$edit_data['id_group'] = $group;
					$edit_data['id_menu'] = $row_edit;
					$edit_data['akses'] = 2;
					$this->db->insert('rel_group',$edit_data);
				}
			}
			else
			{
				$this->db->delete('rel_group',array('id_group'=>$group,'akses'=>2));
				foreach ($edit as $row_edit) 
				{
					$edit_data['id_group'] = $group;
					$edit_data['id_menu'] = $row_edit;
					$edit_data['akses'] = 2;
					$this->db->insert('rel_group',$edit_data);
				}
			}
		}
		else
		{
			$this->db->delete('rel_group',array('id_group'=>$group,'akses'=>2));
		}

		$lihat = $this->input->post('lihat');
		if(!empty($lihat)){
			$lihat_data = array();
			$cek_lihat = $this->db->get_where('rel_group',array('id_group'=>$group,'akses'=>3));
			if($cek_lihat->num_rows() == 0)
			{
				foreach ($lihat as $row_lihat) {
					$lihat_data['id_group'] = $group;
					$lihat_data['id_menu'] = $row_lihat;
					$lihat_data['akses'] = 3;
					$this->db->insert('rel_group',$lihat_data);
				}
			}
			else
			{
				$this->db->delete('rel_group',array('id_group'=>$group,'akses'=>3));
				foreach ($lihat as $row_lihat) {
					$lihat_data['id_group'] = $group;
					$lihat_data['id_menu'] = $row_lihat;
					$lihat_data['akses'] = 3;
					$this->db->insert('rel_group',$lihat_data);
				}
			}
		}
		else
		{
			$this->db->delete('rel_group',array('id_group'=>$group,'akses'=>3));
		}

		$hapus = $this->input->post('hapus');
		if(!empty($hapus)){
			$hapus_data = array();
			$cek_hapus = $this->db->get_where('rel_group',array('id_group'=>$group,'akses'=>4));
			if($cek_hapus->num_rows() == 0)
			{
				foreach ($hapus as $row_hapus) 
				{
					$hapus_data['id_group'] = $group;
					$hapus_data['id_menu'] = $row_hapus;
					$hapus_data['akses'] = 4;
					$this->db->insert('rel_group',$hapus_data);
				}
			}
			else
			{
				$this->db->delete('rel_group',array('id_group'=>$group,'akses'=>4));
				foreach ($hapus as $row_hapus) 
				{
					$hapus_data['id_group'] = $group;
					$hapus_data['id_menu'] = $row_hapus;
					$hapus_data['akses'] = 4;
					$this->db->insert('rel_group',$hapus_data);
				}
			}
		}
		else
		{
			$this->db->delete('rel_group',array('id_group'=>$group,'akses'=>4));
		}
		//$insert = $this->group->save($data);
		echo json_encode(array("status" => TRUE));
	}

}
