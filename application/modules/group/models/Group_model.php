<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	// Datatable server side processing
    public function s_side_datatables_query(){
        $this->db->select('
            groups.id,
            groups.name,
            groups.description,
            groups.created_at, 
            groups.created_by,
            groups.updated_at, 
            groups.updated_by
        ');
        $column_order = array(null, 
            'groups.id',
            'groups.name',
            'groups.description',
            'groups.created_at',
            'groups.created_by',
            'groups.updated_at',
            'groups.updated_by'
        );
        $column_search = array(
            'groups.id',
            'groups.name',
            'groups.description',
            'groups.created_at',
            'groups.created_by',
            'groups.updated_at',
            'groups.updated_by'
        ); 
        $this->db->where('groups.deleted_at','');
        $this->db->from('groups');
        $i = 0;
        foreach ($column_search as $item){
            if($_POST['search']['value']  != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('groups.id', 'asc');
        }
    }
 
    public function s_side_data(){
        $this->s_side_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function count_filtered(){
        $this->s_side_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->where('groups.deleted_at','');
        $this->db->from('groups');
        return $this->db->count_all_results();
    }
    // End datatable server side processing

    public function save($by){
        $data = array(
            'name'          => $this->input->post('nama'),
            'description'   => $this->input->post('deskripsi'),
            'created_at'    => date('Y-m-d H:i:s'),
            'created_by'    => $by
        );
        $query = $this->db->insert('groups', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function edit($id){
        $this->db->select('
            id,
            name,
            description
        ');
        $this->db->where('id', $id);
        $query = $this->db->get('groups');
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function update($id, $by){
        $data = array(
            'name'          => $this->input->post('nama'),
            'description'   => $this->input->post('deskripsi'),
            'updated_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => $by
        );
        $this->db->where('id', $id);
        $this->db->update('groups', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function delete($id, $by){
        $data = array(
            'deleted_at'    => date('Y-m-d H:i:s'),
            'deleted_by'    => $by
        );
        $this->db->where('id', $id);
        $this->db->update('groups', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function delete_menu($id){
        $this->db->where('id_group', $id);
        $this->db->delete('rel_group');
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false; 
        }
    }

    public function group_level($id){
    	$this->db->select('description');
    	$query = $this->db->get_where('groups', array(
            'id'  => $id
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

	public function get_datatables_menu(){
		$data = array(
			'is_trash' => 0,
			'rel' => 0
		);

		$this->db->order_by('urutan','asc');
		$this->db->where($data);
		$query = $this->db->get('menu');
		return $query->result();
	}

	public function get_dropdown_menu_sub($rel){

		$data = array('is_trash' => 0, 'rel' => $rel, 'rel2' => 0);

        $this->db->order_by('urutan','asc');
		$this->db->from('menu');
		$this->db->where($data);
		$query = $this->db->get();

		return $query->result();
	}

	public function get_dropdown_sub2($rel,$rel2){

		$data = array('is_trash' => 0, 'rel' => $rel, 'rel2' => $rel2);

		$this->db->from('menu');
		$this->db->where($data);
		$query = $this->db->get();

		return $query->result();
	}

	public function get_dropdown_menu_sub_cek($rel){

		$data = array('is_trash' => 0, 'rel' => $rel);

		$this->db->from('menu');
		$this->db->where($data);
		$query = $this->db->get();

		return $query->row();
	}

	public function itung_akses($group,$menu,$akses)
	{
		$data = array(
			'id_group' => $group,
			'id_menu' => $menu,
			'akses' => $akses
		);
		$this->db->where($data);
		$query = $this->db->get('rel_group');
		return $query->num_rows();
	}

	public function get_akses($group,$menu,$akses)
	{
		$data = array('id_group' => $group, 'id_menu' => $menu, 'akses' => $akses);
		$this->db->select('*');
		$this->db->where($data);
		$kueri = $this->db->get('rel_group');
		return $kueri;
	}
}
