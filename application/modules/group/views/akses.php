<style>
    .thead-dark tr th{
        color: #fff !important;
    }
    form label{
        margin-top: 5px;
        font-weight: 600 !important;
    }
</style>
<script>
    var level_akses = '<?php echo $title ?>';
</script>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5"><?php echo $title ?></h5>
            </div>
            <div class="d-flex align-items-center">
            </div>
        </div>
    </div>
    <div class="d-flex flex-column-fluid">
        <div class="container">
            <div class="row" style="min-height: 68vh;">
                <div class="col-lg-12 col-xxl-4 order-1 order-xxl-2">
                    <div class="card card-custom card-stretch gutter-b">
                        <div class="card-header border-0">
                            <h3 class="card-title font-weight-bolder text-dark">Daftar <?php echo $title ?></h3>
                        </div>
                        <div class="card-body pt-0">
                            <a href="<?php echo base_url(); ?>group" class="btn btn-light-primary btn-sm btn-rounded"></i> Kembali</a>
                            <div></div>
                            <br>
                            <center>
                                <form id="form">
                                    <input type="hidden" name="id_group" value="<?php echo $this->uri->segment(3); ?>">
                                    <table class="table table table-striped table-vcenter" cellspacing="0" style="width: 100%;">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th style="vertical-align: middle;" rowspan="2">
                                                    <center>No.</center>
                                                </th>
                                                <th style="vertical-align: middle;" rowspan="2">
                                                    Menu
                                                </th>
                                                <th colspan="4">
                                                    <center>Privilages</center>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th width="5%">
                                                    <center>Tambah
                                                        <input type="checkbox" class="tambah" name="all" id="all" onclick="toggle_tambah(this);">
                                                    </center>
                                                </th>
                                                <th width="5%"> 
                                                    <center>Edit
                                                        <input type="checkbox" class="edit" name="all" id="all" onclick="toggle_edit(this);">
                                                    </center>
                                                </th>
                                                <th width="5%">
                                                    <center>Lihat
                                                        <input type="checkbox" class="lihat" name="all" id="all" onclick="toggle_lihat(this);">
                                                    </center>
                                                </th>
                                                <th width="5%">
                                                    <center>Hapus
                                                        <input type="checkbox" class="hapus" name="all" id="all" onclick="toggle_hapus(this);">
                                                    </center>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            $i=0;
                                            foreach ($menu as $row_menu) { 
                                                $i++;
                                                $sub = $this->model->get_dropdown_menu_sub($row_menu->id_menu);
                                                $tambah = 1;
                                                $cek_tambah = $this->model->itung_akses($this->uri->segment(3),$row_menu->id_menu,$tambah);
                                                //print_r($cek_tambah);
                                                $edit = 2;
                                                $cek_edit = $this->model->itung_akses($this->uri->segment(3),$row_menu->id_menu,$edit);
                                                $lihat = 3;
                                                $cek_lihat = $this->model->itung_akses($this->uri->segment(3),$row_menu->id_menu,$lihat);
                                                $hapus = 4;
                                                $cek_hapus = $this->model->itung_akses($this->uri->segment(3),$row_menu->id_menu,$hapus);
                                            ?>
                                            <tr>
                                                <td>
                                                    <center><?php echo $i; ?></center>
                                                </td>
                                                <td>
                                                    <?php echo $row_menu->nama_menu; ?>
                                                </td>
                                                <td>
                                                    <center>
                                                        <input type="checkbox" <?php if($cek_tambah > 0){ ?> checked="checked" <?php } ?> name="tambah[]" class="tambah" id="tambah" value="<?php echo $row_menu->id_menu; ?>">
                                                    </center>
                                                </td>
                                                <td>
                                                    <center>
                                                        <input type="checkbox" <?php if($cek_edit > 0){ ?> checked="checked" <?php } ?> name="edit[]" class="edit" id="edit" value="<?php echo $row_menu->id_menu; ?>">
                                                    </center>
                                                </td>
                                                <td>
                                                    <center>
                                                        <input type="checkbox" <?php if($cek_lihat > 0){ ?> checked="checked" <?php } ?> name="lihat[]" class="lihat" id="lihat" value="<?php echo $row_menu->id_menu; ?>">
                                                    </center>
                                                </td>
                                                <td>
                                                    <center>
                                                        <input type="checkbox" <?php if($cek_hapus > 0){ ?> checked="checked" <?php } ?> name="hapus[]" class="hapus" id="hapus" value="<?php echo $row_menu->id_menu; ?>">
                                                    </center>
                                                </td>
                                            </tr>
                                            <?php 
                                            foreach ($sub as $row_sub) { 

                                                $sub2 = $this->model->get_dropdown_sub2($row_menu->id_menu,$row_sub->id_menu);
                                                
                                                $tambah1 = 1;
                                                $cek_tambah_sub = $this->model->itung_akses($this->uri->segment(3),$row_sub->id_menu,$tambah1);
                                                $edit1 = 2;
                                                $cek_edit_sub = $this->model->itung_akses($this->uri->segment(3),$row_sub->id_menu,$edit1);
                                                $lihat1 = 3;
                                                $cek_lihat_sub = $this->model->itung_akses($this->uri->segment(3),$row_sub->id_menu,$lihat1);
                                                $hapus1 = 4;
                                                $cek_hapus_sub = $this->model->itung_akses($this->uri->segment(3),$row_sub->id_menu,$hapus1);
                                            ?>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <div style="padding-left: 25px;"><?php echo $row_sub->nama_menu; ?></div>
                                                </td>
                                                <td>
                                                    <center>
                                                        <input type="checkbox" <?php if($cek_tambah_sub > 0){ ?> checked="checked" <?php } ?> name="tambah[]" class="tambah" id="tambah" value="<?php echo $row_sub->id_menu; ?>">
                                                    </center>
                                                </td>
                                                <td>
                                                    <center>
                                                        <input type="checkbox" <?php if($cek_edit_sub > 0){ ?> checked="checked" <?php } ?> name="edit[]" class="edit" id="edit" value="<?php echo $row_sub->id_menu; ?>">
                                                    </center>
                                                </td>
                                                <td>
                                                    <center>
                                                        <input type="checkbox" <?php if($cek_lihat_sub > 0){ ?> checked="checked" <?php } ?> name="lihat[]" class="lihat" id="lihat" value="<?php echo $row_sub->id_menu; ?>">
                                                    </center>
                                                </td>
                                                <td>
                                                    <center>
                                                        <input type="checkbox" <?php if($cek_hapus_sub > 0){ ?> checked="checked" <?php } ?> name="hapus[]" class="hapus" id="hapus" value="<?php echo $row_sub->id_menu; ?>">
                                                    </center>
                                                </td>
                                            </tr>
                                            <?php 
                                            foreach ($sub2 as $row_sub2) { 
                                                $tambah2 = 1;
                                                $cek_tambah_sub2 = $this->model->itung_akses($this->uri->segment(3),$row_sub2->id_menu,$tambah);
                                                $sub2 = $this->model->get_dropdown_sub2($row_menu->id_menu,$row_sub->id_menu);
                                                $edit1 = 2;
                                                $cek_edit_sub = $this->model->itung_akses($row_menu->id_menu,$row_sub->id_menu,$edit1);
                                                $lihat1 = 3;
                                                $cek_lihat_sub = $this->model->itung_akses($row_menu->id_menu,$row_sub->id_menu,$lihat1);
                                                $hapus1 = 4;
                                                $cek_hapus_sub = $this->model->itung_akses($row_menu->id_menu,$row_sub->id_menu,$hapus1);
                                            ?>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <center><strong>—— </strong><?php echo $row_sub2->nama_menu; ?></center>
                                                </td>
                                                <td>
                                                    <center>
                                                        <input type="checkbox" <?php if($cek_tambah_sub2 > 0){ ?> checked="checked" <?php } ?> name="tambah[]" class="tambah" id="tambah" value="<?php echo $row_sub2->id_menu; ?>">
                                                    </center>
                                                </td>
                                            </tr>
                                            <?php }}} ?>
                                        </tbody>
                                    </table>
                                </form>
                                <div>
                                    <button type="button" id="btnSave" onclick="save()" class="btn btn-light-success btn-sm btn-rounded">Simpan</button>
                                </div>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo site_url('assets/project/js/auth_akses.js?t=').mt_rand()?>"></script>