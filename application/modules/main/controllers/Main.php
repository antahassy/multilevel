<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct(){
		parent::__construct();
      	$this->load->model('Main_model', 'model');
      	require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
      	date_default_timezone_set('Asia/Jakarta');
	}

	public function login_process(){
		$captcha = $this->input->post('captcha');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$ip_address = $this->input->post('ip_address');
		$check_account = $this->model->check_account($username);

		if($captcha != $this->session->userdata('captcha_sess')){
		    $message['captcha_false'] = true; 
			echo json_encode($message);
		}else{
            if($check_account){
				$status = $check_account->active;
				if($status == 1){
					$process = $this->ion_auth->login($username, $password);
					if($process){
						$this->model->update_ip($username, $ip_address);
						$message['success'] = true; 
						echo json_encode($message);
					}else{
						$message['match'] = true;
						echo json_encode($message);	
					}	
				}else{
					$message['inactive'] = true;
					echo json_encode($message);
				}
			}else{
				$message['username'] = true;
				echo json_encode($message);
			}
		}
	}

	public function logout_process(){
		$process = $this->ion_auth->logout();
		if($process){
			redirect('auth/login');
		}
	}
}