<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_model extends CI_Model {

    public function __construct(){
        parent::__construct(); 
        $this->db = $this->load->database('default', TRUE);
        date_default_timezone_set('Asia/Jakarta');
    }

    public function check_account($identity){
    	$query = $this->db->get_where('users', array('username' => $identity));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
    }

    public function add_user($nip, $nip_baru, $gelar_depan, $nama, $gelar_belakang, $image, $kd_jabatan, $kd_golongan, $kd_unit_organisasi, $kd_unit_kerja, $nm_jabatan, $nm_unit_organisasi, $nm_unit_kerja){
        if($image != ''){
            $foto = $image . '.jpg';
        }else{
            $foto = $image;
        }
        $data = array(
            'username'                  => $nip_baru,
            'nip'                       => $nip,
            'nip_baru'                  => $nip_baru,
            'kd_jabatan'                => $kd_jabatan,
            'nm_jabatan'                => $nm_jabatan,
            'kd_unit_organisasi'        => $kd_unit_organisasi,
            'nm_unit_organisasi'        => $nm_unit_organisasi,
            'kd_unit_kerja'             => $kd_unit_kerja,
            'nm_unit_kerja'             => $nm_unit_kerja,
            'kd_golongan'               => $kd_golongan,
            'gelar_depan'               => $gelar_depan,
            'nama'                      => $nama,
            'gelar_belakang'            => $gelar_belakang,
            'foto'                      => $foto,
            'active'                    => 1,
            'password_update'           => 0
        );
        $query = $this->db->insert('users', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function update_ip($username, $ip_address){
        $data = array(
            'ip_address'  => $ip_address,
            'logged_in'  => date('Y-m-d H:i:s'),
        );
        $this->db->where('username', $username);
        $this->db->update('users', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function username_id($username){
        $this->db->select('id');
        $query = $this->db->get_where('users', array(
            'username'  => $username
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function add_user_group($id_user){
        $data = array(
            'user_id'                   => $id_user,
            'group_id'                  => '2'
        );
        $query = $this->db->insert('users_groups', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }
}