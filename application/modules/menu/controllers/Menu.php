<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	public function __construct(){
		parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Menu_model','model');
		if (! $this->ion_auth->logged_in() || ! $this->ion_auth->is_admin()){
            redirect('auth', 'refresh');
        }
	}

	public function index(){
		$data['title'] = 'Menu';

        $this->load->view('header', $data);
        $this->load->view('index');
        $this->load->view('footer');
	}

	public function s_side_data(){
        $month_format = array (1 => 
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $list = $this->model->s_side_data();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $field->urutan;
            $row[] = $field->nama_menu;
            $row[] = '';
            $row[] = '<a href="' . site_url($field->url) . '" target="_blank">' . $field->url . '</a>';
            $row[] = '<button data="' . $field->id_menu . '" class="btn btn-sm btn-rounded btn-outline-warning" id="btn_edit" title="Edit" style="margin: 2.5px;">Edit</button> <button data="' . $field->id_menu . '" class="btn btn-sm btn-rounded btn-outline-danger" id="btn_delete" title="Hapus" style="margin: 2.5px;">Hapus</button>';

            if($field->created_at != ''){
                $s_created = explode(' ', $field->created_at);
                $s_created_date = explode('-', $s_created[0]);
                $row[] = $s_created_date[2] .'/'. $month_format[(int)$s_created_date[1]] .'/'. $s_created_date[0] . '<br>' . $s_created[1];
            }else{
                $row[] = '';
            }

            if($field->updated_at != ''){
                $s_updated = explode(' ', $field->updated_at);
                $s_updated_date = explode('-', $s_updated[0]);
                $row[] = $s_updated_date[2] .'/'. $month_format[(int)$s_updated_date[1]] .'/'. $s_updated_date[0] . '<br>' . $s_updated[1];
            }else{
                $row[] = '';
            }
 
            $data[] = $row;
            $sub = $this->model->get_dropdown_sub($field->id_menu);
            if($sub){
            	foreach ($sub as $row_sub) {
					$row = array();
					$row[] = '';
					$row[] = '<div style="padding-left: 25px;">' . $row_sub->nama_menu . '</div>';
					$row[] = $row_sub->urutan;
					$row[] = '<a href="' . site_url($row_sub->url) . '" target="_blank">' . $row_sub->url . '</a>';
					$row[] = '<button data="' . $row_sub->id_menu . '" class="btn btn-sm btn-rounded btn-outline-warning" id="btn_edit" title="Edit" style="margin: 2.5px;">Edit</button> <button data="' . $row_sub->id_menu . '" class="btn btn-sm btn-rounded btn-outline-danger" id="btn_delete" title="Hapus" style="margin: 2.5px;">Hapus</button>';

		            if($row_sub->created_at != ''){
		                $s_created = explode(' ', $row_sub->created_at);
		                $s_created_date = explode('-', $s_created[0]);
		                $row[] = $s_created_date[2] .'/'. $month_format[(int)$s_created_date[1]] .'/'. $s_created_date[0] . '<br>' . $s_created[1];
		            }else{
		                $row[] = '';
		            }

		            if($row_sub->updated_at != ''){
		                $s_updated = explode(' ', $row_sub->updated_at);
		                $s_updated_date = explode('-', $s_updated[0]);
		                $row[] = $s_updated_date[2] .'/'. $month_format[(int)$s_updated_date[1]] .'/'. $s_updated_date[0] . '<br>' . $s_updated[1];
		            }else{
		                $row[] = '';
		            }
		 
		            $data[] = $row;
				}
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

	public function save(){
    	$message['type'] = 'disimpan';
    	$by = $this->ion_auth->user()->row()->username;
        $data   = $this->model->save($by);
        if($data){
        	$message['success'] = true;
        	echo json_encode($message);
        }
    }

    public function edit(){
        $id = $this->input->post('id');
        $data = $this->model->edit($id); 
        echo json_encode($data);
    }

    public function update(){
    	$message['type'] = 'diupdate';
        $id = $this->input->post('id_data');
        $by = $this->ion_auth->user()->row()->username;
        $data = $this->model->update($id, $by);
        if($data){
        	$message['success'] = true;
        	echo json_encode($message);
        }
    }

    public function delete(){
        $id = $this->input->post('id');
        $by = $this->ion_auth->user()->row()->username;
        $data = $this->model->delete($id, $by); 
        if($data){
        	$d_menu = $this->model->delete_menu($id);
        	$message['success'] = true;
        	echo json_encode($message);
        }
    }

    public function rel_menu(){
    	$data = $this->model->rel_menu(); 
    	if($data){
    		$object = array();
    		foreach ($data as $row) {
    			$arr = array(
    				'id' 	=> $row->id_menu,
    				'menu' 	=> $row->nama_menu
    			);
    			array_push($object, $arr);
			}
			echo json_encode($object);
    	}else{
    		$message['empty'] = true;
    		echo json_encode($message);
    	}
    }
}
