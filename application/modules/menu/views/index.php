<style>
    .thead-dark tr th{
        color: #fff !important;
    }
    form label{
        margin-top: 5px;
        font-weight: 600 !important;
    }
</style>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5"><?php echo $title ?></h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <button class="btn btn-light-success font-weight-bolder btn-sm" id="btn_add">Tambah Data</button>
            </div>
            <div class="d-flex align-items-center">
            </div>
        </div>
    </div>
    <div class="d-flex flex-column-fluid">
        <div class="container">
            <div class="row" style="min-height: 68vh;">
                <div class="col-lg-12 col-xxl-4 order-1 order-xxl-2">
                    <div class="card card-custom card-stretch gutter-b">
                        <div class="card-header border-0">
                            <h3 class="card-title font-weight-bolder text-dark">Daftar <?php echo $title ?></h3>
                        </div>
                        <div class="card-body pt-0">
                            <table id="table_data" class="table table-striped table-vcenter" style="width: 100%;">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>No</th>
                                        <th>Menu</th>
                                        <th>Urutan</th>
                                        <th>Link</th>
                                        <th>Tindakan</th>
                                        <th>Dibuat</th>
                                        <th>Diupdate</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal animated" id="modal_form" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
            </div>
            <div class="modal-body"> 
                <form id="form_data">
                    <input type="hidden" id="id_data" name="id_data">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Nama Menu<span style="color: red;">*</span></label>
                            <input type="text" name="nama" id="nama" class="form-control">

                            <label>Url Menu<span style="color: red;">*</span></label>
                            <input type="text" name="url" id="url" class="form-control">

                            <label>Rel Menu<!-- <span style="color: red;">*</span> --></label>
                            <select class="form-control" name="s_rel" id="s_rel">
                                <option value="">Pilih Rel</option>
                            </select>

                            <label>Urutan Menu<span style="color: red;">*</span></label>
                            <input type="text" name="urutan" id="urutan" class="form-control">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-outline-primary" id="btn_process"></button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo site_url('assets/project/js/auth_menu.js?t=').mt_rand()?>"></script>
