<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paket_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	// Datatable server side processing
    public function s_side_datatables_query(){
        $this->db->select('
            tb_paket.id_paket,
            tb_paket.nama,
            tb_paket.nilai,
            tb_paket.icon,
            tb_paket.created_by,
            tb_paket.created_at,
            tb_paket.updated_by,
            tb_paket.updated_at
        ');
        $column_order = array(null, 
            'tb_paket.id_paket',
            'tb_paket.nama',
            'tb_paket.nilai',
            'tb_paket.icon',
            'tb_paket.created_by',
            'tb_paket.created_at',
            'tb_paket.updated_by',
            'tb_paket.updated_at'
        );
        $column_search = array(
            'tb_paket.id_paket',
            'tb_paket.nama',
            'tb_paket.nilai',
            'tb_paket.icon',
            'tb_paket.created_by',
            'tb_paket.created_at',
            'tb_paket.updated_by',
            'tb_paket.updated_at'
        ); 
        $this->db->where('tb_paket.deleted_at','');
        $this->db->from('tb_paket');
        $i = 0;
        foreach ($column_search as $item){
            if($_POST['search']['value']  != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('tb_paket.id_paket', 'asc');
        }
    }

    public function s_side_data(){
        $this->s_side_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function count_filtered(){
        $this->s_side_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->where('tb_paket.deleted_at','');
        $this->db->from('tb_paket');
        return $this->db->count_all_results();
    }
    // End datatable server side processing

	public function save($by){
        $value = str_replace('.', '', $this->input->post('nilai_paket'));
        $data = array(
            'nama'          => $this->input->post('nm_paket'),
            'nilai'         => $value,
            'icon'          => $this->input->post('s_icon'),
            'created_at'    => date('Y-m-d H:i:s'),
            'created_by'    => $by
        );
        $query = $this->db->insert('tb_paket', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function edit($id){
        $this->db->select('
            id_paket,
            nama,
            nilai,
            icon
        ');
        $this->db->where('id_paket', $id);
        $query = $this->db->get('tb_paket');
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function update($id, $by){
        $value = str_replace('.', '', $this->input->post('nilai_paket'));
        $data = array(
            'nama'          => $this->input->post('nm_paket'),
            'nilai'         => $value,
            'icon'          => $this->input->post('s_icon'),
            'updated_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => $by
        );
        $this->db->where('id_paket', $id);
        $this->db->update('tb_paket', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function delete($id, $by){
        $data = array(
            'deleted_at'    => date('Y-m-d H:i:s'),
            'deleted_by'    => $by
        );
        $this->db->where('id_paket', $id);
        $this->db->update('tb_paket', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }
}
