<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Process extends CI_Controller {

	public function __construct(){
		parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Process_model','model');
		if (! $this->ion_auth->logged_in()){
            redirect('auth', 'refresh');
        }
	}

    public function pairing(){
        $tgl = date('Y-m-d H:i:s');
        $tanggal = date('Y-m-d');
        $data = $this->model->tree_data();
        if($data){
            $main_data = $data;
            foreach($data as $row){
                $primary = $row->id_user;
                $uplines = $row->id_upline;
                $arr_downlink = array();
                foreach ($main_data as $sub_row) {
                    if((int)$sub_row->id_user >= (int)$primary){
                        array_push($arr_downlink, array(
                            'id_user'   => $sub_row->id_user,
                            'id_upline' => $sub_row->id_upline,
                            'nilai'     => $sub_row->nilai
                        ));
                    }
                }
                $sub_data = array();
                foreach ($arr_downlink as $key) {
                    array_push($sub_data, $key['id_upline']);
                }
                $sub_data = array_unique($sub_data);
                $level = array();
                foreach ($sub_data as $row_data) {
                    $n_level = array();
                    if($row_data == $primary){
                        foreach ($arr_downlink as $first) {
                            if($first['id_user'] == $primary){
                                if($first['nilai'] == null){
                                    array_push($n_level, array(
                                        'id_user' => $primary,
                                        'nilai' => '0'
                                    ));
                                }else{
                                    array_push($n_level, array(
                                        'id_user' => $primary,
                                        'nilai' => $first['nilai']
                                    ));
                                }
                            }
                        }
                        array_push($level, $n_level);
                    }
                    if(count($level) > 0){
                        $last = count($level) - 1;
                        $object = array();
                        foreach ($level[$last] as $row) {
                            foreach ($arr_downlink as $key) {
                                if($key['id_upline'] == $row['id_user']){
                                    if($key['nilai'] == null){
                                        array_push($object, array(
                                            'id_user' => $key['id_user'],
                                            'nilai' => '0'
                                        ));
                                    }else{
                                        array_push($object, array(
                                            'id_user' => $key['id_user'],
                                            'nilai' => $key['nilai']
                                        ));
                                    }
                                }
                            }
                        }
                        array_push($level, $object);
                    }
                }
                $arr_level = array();
                foreach ($level as $l) {
                    if(count($l) == 0){
                        unset($l);
                    }else{
                        array_push($arr_level, $l);
                    }
                }
                $value = array(); 
                for($i = 0; $i < count($arr_level); $i ++){
                    foreach ($arr_level[$i] as $pair) {
                        array_push($value, (int)$pair['nilai']);
                    }
                }
                if(count($value) == 0){
                    array_push($value, (int)$row->nilai);
                }
                $value = array_sum($value);
                $new_data[] = array(
                    'id_user'   => $primary,
                    'id_upline' => $uplines,
                    'value'   => $value
                );
            }
            $limit = 0;
            foreach ($new_data as $calc) {
                $user_value = $this->model->user_value($calc['id_user']);
                if($user_value->value == null){
                    $save_value = $this->model->save_value($calc['id_user'], $calc['id_upline'], $calc['value'], $tgl);
                }else{
                    $calc['value'] = $calc['value'] - $user_value->value;
                    if($calc['value'] < 0){
                        $calc['value'] = 0;
                    }else{
                        $calc['value'] = $calc['value'];
                    }
                    $save_value = $this->model->save_value($calc['id_user'], $calc['id_upline'], $calc['value'], $tgl);
                }
                $limit ++;
            }
            if($limit == count($new_data)){
                $count_value = $this->model->count_value();
                $start_limit = $count_value - $limit;
                $today_value = $this->model->today_value($limit, $start_limit);
                foreach($data as $row) {
                    $primary = $row->id_user;
                    $arr_pairing = array();
                    foreach ($today_value as $today) {
                        if($primary == $today->id_upline){
                            array_push($arr_pairing, array(
                                'id_user'   => $today->id_user,
                                'value'   => $today->value
                            ));
                        }
                    }
                    if(count($arr_pairing) == 2){
                        $last_pairing = $this->model->last_bonus_pairing($primary);
                        if($last_pairing){
                            $id_kiri = $arr_pairing[0]['id_user'];
                            $kiri = $arr_pairing[0]['value'] + $last_pairing->sisa_kiri;

                            $id_kanan = $arr_pairing[1]['id_user'];
                            $kanan = $arr_pairing[1]['value'] + $last_pairing->sisa_kanan;
                        }else{
                            $id_kiri = $arr_pairing[0]['id_user'];
                            $sum_kiri = $this->model->user_value($id_kiri);
                            $kiri = $sum_kiri->value;

                            $id_kanan = $arr_pairing[1]['id_user'];
                            $sum_kanan = $this->model->user_value($id_kanan);
                            $kanan = $sum_kanan->value;
                        }
                        $min_pairing = min($kiri, $kanan);
                        $sisa_kiri = $kiri - $min_pairing;
                        $sisa_kanan = $kanan - $min_pairing;
                        $bonus_pairing = 10 * ((int)$min_pairing / 100);

                        $arr_bonus_pairing[] = array(
                            'id_user'       => $primary,
                            'tanggal'       => $tanggal,
                            'id_kiri'       => $id_kiri,
                            'kiri'          => $kiri,
                            'sisa_kiri'     => $sisa_kiri,
                            'id_kanan'      => $id_kanan,
                            'kanan'         => $kanan,
                            'sisa_kanan'    => $sisa_kanan,
                            'pairing'       => $min_pairing,
                            'bonus'         => $bonus_pairing,
                            'created_at'    => date('Y-m-d H:i:s')
                        );
                    }
                }
                $save_bonus_pairing = $this->db->insert_batch('tb_bonus_pairing', $arr_bonus_pairing);
                if($save_bonus_pairing){
                    $message['success'] = true;
                    echo json_encode($message);
                }
            }
        }
    }
}
