<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Process_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

    public function tree_data(){
        $this->db->order_by('tb_member.id_member', 'asc'); 
        $this->db->select('
            tb_member.id_upline,
            tb_member.id_user,
            tb_paket.nilai
        ');
        $this->db->where('tb_member.deleted_at','');
        $this->db->from('tb_member');
        $this->db->join('tb_paket', 'tb_member.id_paket = tb_paket.id_paket', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }

    public function user_value($id_user){
        $this->db->select_sum('value');
        $query = $this->db->get_where('tb_value', array(
            'id_user' => $id_user
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function save_value($id_user, $id_upline, $value, $tgl){
        $data = array(
            'id_user'   => $id_user,
            'id_upline' => $id_upline,
            'tanggal'   => $tgl,
            'value'     => $value
        );
        $query = $this->db->insert('tb_value', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function count_value(){
        $this->db->from('tb_value');
        return $this->db->count_all_results();
    }

    public function today_value($limit, $start_limit){
        $this->db->limit($limit, $start_limit);
        $this->db->select('
            id_user,
            id_upline,
            value
        ');
        $query = $this->db->get('tb_value');
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }

    public function last_bonus_pairing($id_user){
        $this->db->order_by('id_bonus_pairing', 'desc');
        $this->db->select('sisa_kiri, sisa_kanan');
        $query = $this->db->get_where('tb_bonus_pairing', array(
            'id_user' => $id_user
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }
}
