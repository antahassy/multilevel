<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

	public function __construct(){
		parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Setting_model','model');
		$this->load->model('Group/Group_model','group');
        if (! $this->ion_auth->logged_in() || ! $this->ion_auth->is_admin()){
            redirect('auth', 'refresh');
        }
	}

	public function index(){
        $data['title'] = 'Setting';

        $this->load->view('header', $data);
        $this->load->view('index');
        $this->load->view('footer');
	}


	public function update(){
        $by = $this->ion_auth->user()->row()->username;
        $val_1 = $this->input->post('judul_web');
        $u_1 = $this->model->update_1(1, $val_1, $by);

        $val_2 = $this->input->post('deskripsi');
        $u_2 = $this->model->update_2(2, $val_2, $by);

        $val_4 = $this->input->post('judul_navbar');
        $u_4 = $this->model->update_4(4, $val_4, $by);

        $config['upload_path']   = './assets/indokordsa';
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['max_size']      = 0;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('image')){
            $file   = array('image' => $this->upload->data());
            $val_3  = $file['image']['file_name'];
            $config['image_library']    = 'gd2';
            $config['source_image']     = './assets/indokordsa/'.$val_3;
            $config['create_thumb']     = FALSE;
            $config['maintain_ratio']   = FALSE;
            $config['quality']          = '75%';
            $config['width']            = 800;
            $config['height']           = 600;
            $config['new_image']        = './assets/indokordsa/'.$val_3;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $u_3   = $this->model->update_3(3, $val_3, $by); 
            if($u_3){
                $last_image = $this->input->post('get_image');
                $delete_image = $this->input->post('delete_image');
                if($last_image != ''){
                    unlink('./assets/indokordsa/'.$last_image);
                }elseif($delete_image != ''){
                    unlink('./assets/indokordsa/'.$delete_image);
                }
            }
        }else{
            $last_image = $this->input->post('get_image');
            $delete_image = $this->input->post('delete_image');
            if($last_image == '' && $delete_image != ''){
                unlink('./assets/indokordsa/'.$delete_image);
                $val_3  = '';
                $u_3   = $this->model->update_3(3, $val_3, $by);
            }elseif($last_image == '' && $delete_image == ''){
                $val_3  = '';
                $u_3   = $this->model->update_3(3, $val_3, $by);
            }else{
                $val_3  = $last_image;
                $u_3   = $this->model->update_3(3, $val_3, $by);
            }
        }

        if($u_1 && $u_2 && $u_3 && $u_4){
            $message['success'] = true;
            echo json_encode($message);
        }
    }

}
