 <?php 
$setting = $this->Ref_model->ref_setting()->result_array();
$judul_navbar = $setting[3]['value'];
$judul_web = $setting[0]['value'];
$deskripsi = $setting[1]['value'];
$logo = $setting[2]['value'];
?>
<style>
    .thead-dark tr th{
        color: #fff !important;
    }
    form label{
        margin-top: 5px;
        font-weight: 600 !important;
    }
</style>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5"><?php echo $title ?></h5>
            </div>
            <div class="d-flex align-items-center">
            </div>
        </div>
    </div>
    <div class="d-flex flex-column-fluid">
        <div class="container">
            <div class="row" style="min-height: 68vh;">
                <div class="col-lg-12 col-xxl-4 order-1 order-xxl-2">
                    <div class="card card-custom card-stretch gutter-b">
                        <div class="card-header border-0">
                            <h3 class="card-title font-weight-bolder text-dark">Daftar <?php echo $title ?></h3>
                        </div>
                        <div class="card-body pt-0">
                            <form id="form">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Judul Navigasi</label>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 5px;">
                                        <input name="judul_navbar" type="text" class="form-control" id="judul_nav" value="<?php echo $judul_navbar; ?>">
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Judul Website</label>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 5px;">
                                        <input name="judul_web" type="text" class="form-control" id="judul_web" value="<?php echo $judul_web; ?>">
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Deskripsi Website</label>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 5px;">
                                        <textarea name="deskripsi" id="deskripsi" class="form-control" cols="30" rows="5"><?php echo $deskripsi; ?></textarea>
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Logo Website</label>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 5px;">
                                        <input type="hidden" name="delete_image" id="delete_image" value="<?php echo $logo?>">
                                        <input type="hidden" name="get_image" id="get_image" value="<?php echo $logo?>">
                                        <input type="file" name="image" id="image" accept="image/*" style="width: 100%;">
                                        <div>
                                            <div id="delete_preview_items" style="display: block;">Hapus Gambar</div>
                                            <img id="preview_items" src="<?php echo($logo == NULL)?site_url('assets/project/blank.png'):site_url('assets/project/'.$logo); ?>">
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <br>
                            <center>
                                <div class="col-md-12">
                                    <button type="button" id="btn_process" class="btn btn-light-success btn-sm btn-rounded">Update</button>
                                </div>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo site_url('assets/project/js/auth_setting.js?t=').mt_rand()?>"></script>