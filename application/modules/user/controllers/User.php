<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
		parent::__construct();
        $this->load->model('User_model', 'model'); 
        date_default_timezone_set('Asia/Jakarta');
        if (! $this->ion_auth->logged_in() || ! $this->ion_auth->is_admin()){
            redirect('auth', 'refresh');
        }
	} 

	public function index(){ 
        $data['title'] = 'User';

        $this->load->view('header', $data);
        $this->load->view('index');
        $this->load->view('footer');
	}

    public function s_side_data(){
        $month_format = array (1 => 
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $list = $this->model->s_side_data();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->username;
            if($field->groups_deleted != ''){
                $row[] = '';
            }else{
                $row[] = $field->description;
            }
            $row[] = $field->nama;

            if($field->active == 0){
                $row[] = '<button id="btn_activated" data="' . $field->id . '" alt="' . $field->active . '" class="btn btn-sm btn-rounded btn-outline-danger">Non Aktif</button>';
            }else{
                $row[] = '<button id="btn_activated" data="' . $field->id . '" alt="' . $field->active . '" class="btn btn-sm btn-rounded btn-outline-primary">Aktif</button>';
            }
            $row[] = '<button id="btn_edit" style="margin: 2.5px;" class="btn btn-sm btn-rounded btn-outline-warning" data="' . $field->id . '" title="Edit">Edit</button>';

            if($field->created_at != ''){
                $s_created = explode(' ', $field->created_at);
                $s_created_date = explode('-', $s_created[0]);
                $row[] = $s_created_date[2] .'/'. $month_format[(int)$s_created_date[1]] .'/'. $s_created_date[0] . '<br>' . $s_created[1];
            }else{
                $row[] = '';
            }

            if($field->updated_at != ''){
                $s_updated = explode(' ', $field->updated_at);
                $s_updated_date = explode('-', $s_updated[0]);
                $row[] = $s_updated_date[2] .'/'. $month_format[(int)$s_updated_date[1]] .'/'. $s_updated_date[0] . '<br>' . $s_updated[1];
            }else{
                $row[] = '';
            }
 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function save(){
        $username = $this->input->post('username');
        $checking = $this->model->check_username($username);
        if($checking){
            $message['account'] = true;
            echo json_encode($message);
        }else{
            $password = $this->input->post('password');
            $s_level = $this->input->post('s_level');
            $by = $this->ion_auth->user()->row()->username;

            $message['type'] = 'disimpan';
            $config['upload_path']   = './assets/user/image';
            $config['allowed_types'] = 'jpg|png|jpeg|gif';
            $config['max_size']      = 0;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if($this->upload->do_upload('image')){
                $file   = array('image' => $this->upload->data());
                $image  = $file['image']['file_name'];
                $config['image_library']    = 'gd2';
                $config['source_image']     = './assets/user/image/'.$image;
                $config['create_thumb']     = FALSE;
                $config['maintain_ratio']   = FALSE;
                $config['quality']          = '75%';
                $config['width']            = 800;
                $config['height']           = 600;
                $config['new_image']        = './assets/user/image/'.$image;
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $data   = $this->model->save($image, $by, $username); 
                if($data){
                    $user = $this->model->identity_id($username);
                    $this->ion_auth->reset_password($username, $password);
                    $level_grup = $this->model->save_user_grup($user->id, $s_level);
                    if($level_grup){
                        $message['success'] = true;
                        echo json_encode($message);
                    }
                }
            }else{
                $image = '';
                $data   = $this->model->save($image, $by, $username);
                if($data){
                    $user = $this->model->identity_id($username);
                    $this->ion_auth->reset_password($username, $password);
                    $level_grup = $this->model->save_user_grup($user->id, $s_level);
                    if($level_grup){
                        $message['success'] = true;
                        echo json_encode($message);
                    }
                }
            }
        }
    }

    public function edit(){
        $id = $this->input->post('id');
        $data = $this->model->edit($id); 
        $level = $this->model->user_grup($id);
        $data->level = $level->group_id;
        echo json_encode($data);
    }

    public function update(){
        $id_user = $this->input->post('id_data');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $s_level = $this->input->post('s_level');
        if($password != ''){
            $this->ion_auth->reset_password($username, $password);
        }
        $by = $this->ion_auth->user()->row()->username;
        $this->model->update_user_grup($id_user, $s_level);
        $message['type'] = 'diupdate';
        $config['upload_path']   = './assets/user/image';
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['max_size']      = 0;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('image')){
            $file   = array('image' => $this->upload->data());
            $image  = $file['image']['file_name'];
            $config['image_library']    = 'gd2';
            $config['source_image']     = './assets/user/image/'.$image;
            $config['create_thumb']     = FALSE;
            $config['maintain_ratio']   = FALSE;
            $config['quality']          = '75%';
            $config['width']            = 800;
            $config['height']           = 600;
            $config['new_image']        = './assets/user/image/'.$image;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $data   = $this->model->update($id_user, $image, $by); 
            if($data){
                $last_image = $this->input->post('get_image');
                $delete_image = $this->input->post('delete_image');
                if($last_image != ''){
                    unlink('./assets/user/image/'.$last_image);
                    $message['success'] = true;
                    echo json_encode($message);
                }elseif($delete_image != ''){
                    unlink('./assets/user/image/'.$delete_image);
                    $message['success'] = true;
                    echo json_encode($message);
                }else{
                    $message['success'] = true;
                    echo json_encode($message);
                }
            }
        }else{
            $last_image = $this->input->post('get_image');
            $delete_image = $this->input->post('delete_image');
            if($last_image == '' && $delete_image != ''){
                unlink('./assets/user/image/'.$delete_image);
                $image  = '';
                $data   = $this->model->update($id_user, $image, $by);
                if($data){
                    $message['success'] = true;
                    echo json_encode($message);
                }
            }elseif($last_image == '' && $delete_image == ''){
                $image  = '';
                $data   = $this->model->update($id_user, $image, $by);
                if($data){
                    $message['success'] = true;
                    echo json_encode($message);
                }
            }else{
                $image  = $last_image;
                $data   = $this->model->update($id_user, $image, $by);
                if($data){
                    $message['success'] = true;
                    echo json_encode($message);
                }
            }
        }
    }

    public function aktifkan(){
        $id = $this->input->post('id');
        $data = $this->model->aktifkan($id);
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }

    public function non_aktifkan(){
        $id = $this->input->post('id');
        $data = $this->model->non_aktifkan($id);
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }

    public function level_grup(){
    	$data = $this->model->level_grup();
    	echo json_encode($data);
    }
}