<?php
$setting = $this->Ref_model->ref_setting()->result_array();
$data['judul_navbar'] = $setting[3]['value'];
$data['judul_web'] = $setting[0]['value'];
$data['deskripsi'] = $setting[1]['value'];
$data['logo'] = $setting[2]['value'];
?>
<!DOCTYPE html>
<html lang="en">
    <!--begin::Head-->
    <head><base href="../../../../">
        <meta charset="utf-8" />
        <title><?php echo $data['judul_web']; ?> - <?php echo $title; ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
        <meta name="description" content="<?php echo $data['judul_web'] . '. ' . $data['deskripsi'] ?>">
        <meta name="keywords" content="Multi,Level,Member,Bonus,Sponsor,Upline,Tree">
        <meta name="googlebot-news" content="index,follow">
        <meta name="googlebot" content="index,follow">
        <meta name="author" content="Antahassy Wibawa">
        <meta name="robots" content="index,follow">
        <meta name="language" content="id">
        <meta name="Classification" content="Level">
        <meta name="geo.country" content="Indonesia">
        <meta name="geo.placename" content="Indonesia"> 
        <meta name="geo.position" content="-6.5899176; 106.8230479">
        <meta http-equiv="content-language" content="In-Id">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Cache-Control" content="no-cache">
        <meta http-equiv="Copyright" content="<?php echo $data['judul_web']?>">
        <meta property="og:title" content="<?php echo $data['judul_web']?>">
        <meta property="og:url" content="http://127.0.0.1:8000/">
        <meta property="og:type" content="JobList">
        <meta property="og:site_name" content="<?php echo $data['judul_web']?>">
        <meta itemprop="name" content="<?php echo $data['judul_web']?>">
        <link rel="canonical" href="https://keenthemes.com/metronic" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
        <link href="<?php echo site_url('assets/metronic_login/css/pages/login/classic/login-5.css?t=').mt_rand()?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url('assets/metronic_login/plugins/global/plugins.bundle.css?t=').mt_rand()?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url('assets/metronic_login/plugins/custom/prismjs/prismjs.bundle.css?t=').mt_rand()?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url('assets/metronic_login/css/style.bundle.css?t=').mt_rand()?>" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="<?php echo site_url('assets/project/' . $data['logo'] . '?t=').mt_rand()?>" />

        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/antahassy.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/dataTables.bootstrap4.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/buttons.bootstrap4.min.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/dataTables.buttons.min.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/jquery-ui.min.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/sweetalert2.min.css?t=').mt_rand()?>">

        <script>var HOST_URL = "<?php echo site_url()?>";</script>
        <script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#0BB783", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#D7F9EF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
        <script src="<?php echo site_url('assets/metronic_login/plugins/global/plugins.bundle.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/metronic_login/plugins/custom/prismjs/prismjs.bundle.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/metronic_login/js/scripts.bundle.js?t=').mt_rand()?>"></script>

        <script src="<?php echo site_url('assets/antahassy/js/jquery.dataTables.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/dataTables.bootstrap4.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/dataTables.buttons.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/buttons.print.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/buttons.html5.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/buttons.flash.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/buttons.colVis.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/be_tables_datatables.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/zip.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/pdfmake.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/vfs_fonts.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/jquery-ui.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/sweetalert2.all.min.js?t=').mt_rand()?>"></script>
    </head>
    <style>
        body, body.swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown){
            overflow-y: auto !important;
            padding-right: 0 !important;
            min-height: 100vh;
        }
        .swal2-popup{
            width: 30em !important;
            padding: inherit !important;
            border-radius: 0.5em !important;
        }
        .swal2-popup .swal2-footer{
            margin: 0 !important;
            padding: 0 !important;
            border-top: none !important;
        }
        .swal2-icon{
            margin: 5px !important;
        }
        .swal2-actions{
            margin: 5px !important;
        }
        .swal2-actions button{
            font-weight: 600 !important;
        }
        .swal2-container.swal2-shown {
            background-color: rgba(0,0,0,0.75);
        }
        .swal2-popup .swal2-content{
            max-height: 200px !important;
            overflow-y: auto !important;
        }
        .swal2-popup .swal2-styled:focus{
            outline: 0;
            box-shadow: none !important;
        }
        .swal2-popup .swal2-styled.swal2-confirm{
            background-color: blue !important;
        }
        .swal2-popup .swal2-confirm:hover, .swal2-confirm:focus{
            background-color: rgb(0,0,100) !important;
        }
        .swal2-popup .swal2-styled.swal2-cancel{
            background-color: red !important;
        }
        .swal2-popup .swal2-cancel:hover, .swal2-cancel:focus{
            background-color: rgb(100,0,0) !important;
        }
        .swal2-image{
            height: 100px;
            width: 100px;
            border-radius: 100%;
        }
        pre{
            font-family: verdana !important; 
            font-size: 14px !important;
            color: #fff;
        }
        .ui-datepicker{
            z-index: 99999 !important;
        }
        #ui-datepicker-div{
            width: auto;
            left: calc(50% - 300px / 2) !important;
            font-size: 15px !important;
        }
        .ui-autocomplete{
            z-index: 99999 !important;
            max-height: 300px !important;
            overflow-y: auto !important;
            overflow-x: hidden !important;
        }
    </style>
    <script>
        var site = "<?php echo site_url('')?>";
    </script>
    <body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled page-loading">
        <div class="d-flex flex-column flex-root">
            <div class="login login-5 login-signin-on d-flex flex-row-fluid" id="kt_login">
                <div class="d-flex flex-center bgi-size-cover bgi-no-repeat flex-row-fluid" style="background-image: url(<?php echo site_url('assets/media/bg/bg-2.jpg')?>);">
                    <div class="login-form text-center text-white p-7 position-relative overflow-hidden">
                        <div class="d-flex flex-center mb-15">
                            <a href="#">
                                <img src="<?php echo site_url('assets/project/logo.png?t=')?>" alt="Text Logo" style="width: 100%;" />
                            </a>
                        </div>
                        <div class="login-signin">
                            <form id="form_login">
                                <input type="hidden" name="ip_address" id="ip_address">
                                <div class="form-group">
                                    <input class="form-control h-auto text-white bg-white-o-5 rounded-pill border-0 py-4 px-8" type="text" placeholder="Username" name="username" id="username">
                                </div>
                                <div class="form-group">
                                    <input class="form-control h-auto text-white bg-white-o-5 rounded-pill border-0 py-4 px-8" type="password" placeholder="Password" name="password" id="password">
                                </div>
                                <div class="form-group">
                                    <!-- <div style="text-align: center; border: 1px solid black; padding: 25px; background-color: rgba(0, 0, 0, 0.75); color: #fff;">
                                        <?php 
                                            $arrs = str_split($random_str);
                                            echo implode (" ", $arrs);
                                        ?>
                                    </div> -->
                                    <div><?php echo $captcha ?></div>
                                    <div style="text-align: center; cursor: pointer; font-weight: 800; margin-bottom: 15px;" onclick="window.location.reload(true)"><i>Perbaharui Captcha</i></div>
                                    <input type="text" class="form-control h-auto text-white bg-white-o-5 rounded-pill border-0 py-4 px-8" id="captcha" name="captcha" placeholder="Captcha">
                                </div>
                                <div class="form-group text-center mt-10">
                                    <button id="btn_login" type="submit" class="btn btn-pill btn-primary opacity-90 px-15 py-3">Login</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="<?php echo site_url('assets/project/js/auth_login.js?t=').mt_rand()?>"></script>
    </body>
</html>