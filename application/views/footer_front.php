    <style type="text/css">
        .contact_icon{
            position: fixed;
            bottom: 0;
            right: 0;
        }
        .home_contact{
            border-color: transparent;
            cursor: pointer;
            padding: 10px;
            border-radius: 100%;
        }
        .right_icon{
            width: 50px;
            font-size: 40px;
        }
        @media screen and (max-width: 767px){
            .right_icon{
                width: 40px;
                font-size: 35px;
            }
        }
    </style>
    <div class="contact_icon" id="contact_icon">
        <div class="home_contact" style="display: grid;">
            <a class="whatsapp" style="margin: 5px;">
                <img src="<?php echo site_url('assets/antahassy/image/wa.png')?>" class="right_icon">
            </a>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            navbar_click();
            //mobile_click();
            contact();
            function navbar_click(){
                $('.m_beranda').on('click', function(){
                    $('html, body').animate({
                        scrollTop: $('#c_beranda').offset().top
                    }, 1500);
                    $('.nav-menu').children().removeClass('menu-active');
                    $(this).addClass('menu-active');
                });
                $('.m_fitur').on('click', function(){
                    $('html, body').animate({
                        scrollTop: $('#c_fitur').offset().top
                    }, 1500);
                    $('.nav-menu').children().removeClass('menu-active');
                    $(this).addClass('menu-active');
                });
                $('.m_biaya').on('click', function(){
                    $('html, body').animate({
                        scrollTop: $('#c_biaya').offset().top
                    }, 1500);
                    $('.nav-menu').children().removeClass('menu-active');
                    $(this).addClass('menu-active');
                });
                $('.m_kontak').on('click', function(){
                    $('html, body').animate({
                        scrollTop: $('#c_kontak').offset().top
                    }, 1500);
                    $('.nav-menu').children().removeClass('menu-active');
                    $(this).addClass('menu-active');
                });
            }
            function contact(){
                var mobile_app = {
                    Android: function() {
                        return navigator.userAgent.match(/Android/i);
                    },
                    iOS: function() {
                        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
                    },
                    any: function() {
                        return (mobile_app.Android() || mobile_app.iOS());
                    }
                };
                $(".whatsapp").on("click", function(){
                    var whatsapp_contact = 62816722268;
                    if(mobile_app.any()){
                        var whatsapp_API_url = "whatsapp://send";
                        $(this).attr('href', whatsapp_API_url + '?phone=' + whatsapp_contact + "&text=Hallo%20saya%20dapat%20info%20dari%20website%20TAMA%20absensi%20online,%20saya%20sedang%20mencari...");
                    }else{
                        var whatsapp_API_url = "https://web.whatsapp.com/send";
                        $(this).attr('target', '_blank');
                        $(this).attr('href', whatsapp_API_url +'?phone=' + whatsapp_contact + "&text=Hallo%20saya%20dapat%20info%20dari%20website%20TAMA%20absensi%20online,%20saya%20sedang%20mencari...");
                    }
                });
            }
        });
    </script>
</body>
</html>