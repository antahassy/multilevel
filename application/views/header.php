<?php
$setting = $this->Ref_model->ref_setting()->result_array();
$data['judul_navbar'] = $setting[3]['value'];
$data['judul_web'] = $setting[0]['value'];
$data['deskripsi'] = $setting[1]['value'];
$data['logo'] = $setting[2]['value'];
?>
<!DOCTYPE html>
<html lang="en">
    <head><base href="">
        <title><?php echo $data['judul_web']; ?> - <?php echo $title; ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
        <meta name="description" content="<?php echo $data['judul_web'] . '. ' . $data['deskripsi'] ?>">
        <meta name="keywords" content="Multi,Level,Member,Bonus,Sponsor,Upline,Tree">
        <meta name="googlebot-news" content="index,follow">
        <meta name="googlebot" content="index,follow">
        <meta name="author" content="Antahassy Wibawa">
        <meta name="robots" content="index,follow">
        <meta name="language" content="id">
        <meta name="Classification" content="Level">
        <meta name="geo.country" content="Indonesia">
        <meta name="geo.placename" content="Indonesia"> 
        <meta name="geo.position" content="-6.5899176; 106.8230479">
        <meta http-equiv="content-language" content="In-Id">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Cache-Control" content="no-cache">
        <meta http-equiv="Copyright" content="<?php echo $data['judul_web']?>">
        <meta property="og:title" content="<?php echo $data['judul_web']?>">
        <meta property="og:url" content="http://127.0.0.1:8000/">
        <meta property="og:type" content="JobList">
        <meta property="og:site_name" content="<?php echo $data['judul_web']?>">
        <meta itemprop="name" content="<?php echo $data['judul_web']?>">

        <link rel="shortcut icon" href="<?php echo site_url('assets/project/' . $data['logo'] . '?t=').mt_rand()?>" />

        <link rel="canonical" href="https://keenthemes.com/metronic" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
        <link href="<?php echo site_url('assets/metronic/css/fullcalendar.bundle.css?t=').mt_rand()?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url('assets/metronic/css/plugins.bundle.css?t=').mt_rand()?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url('assets/metronic/css/prismjs.bundle.css?t=').mt_rand()?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url('assets/metronic/css/style.bundle.css?t=').mt_rand()?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url('assets/metronic/css/layout/base/light.css?t=').mt_rand()?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url('assets/metronic/css/layout/menu/light.css?t=').mt_rand()?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url('assets/metronic/css/layout/brand/dark.css?t=').mt_rand()?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url('assets/metronic/css/layout/aside/dark.css?t=').mt_rand()?>" rel="stylesheet" type="text/css" />

        <script>var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";</script>
        <script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1400 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#3699FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#E4E6EF", "dark": "#181C32" }, "light": { "white": "#ffffff", "primary": "#E1F0FF", "secondary": "#EBEDF3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#3F4254", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#EBEDF3", "gray-300": "#E4E6EF", "gray-400": "#D1D3E0", "gray-500": "#B5B5C3", "gray-600": "#7E8299", "gray-700": "#5E6278", "gray-800": "#3F4254", "gray-900": "#181C32" } }, "font-family": "Poppins" };</script>
        <script src="<?php echo site_url('assets/metronic/js/plugins.bundle.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/metronic/js/prismjs.bundle.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/metronic/js/scripts.bundle.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/metronic/js/fullcalendar.bundle.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/metronic/js/widgets.js?t=').mt_rand()?>"></script>

        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/antahassy.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/animation.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/dataTables.bootstrap4.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/buttons.bootstrap4.min.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/dataTables.buttons.min.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/jquery-ui.min.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/sweetalert2.min.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/treeflex.css?t=').mt_rand()?>">

        <script src="<?php echo site_url('assets/antahassy/js/jquery.dataTables.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/dataTables.bootstrap4.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/dataTables.buttons.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/buttons.print.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/buttons.html5.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/buttons.flash.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/buttons.colVis.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/be_tables_datatables.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/zip.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/pdfmake.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/vfs_fonts.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/jquery-ui.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/chart.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/chart.plugin.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/sweetalert2.all.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/treeify.js?t=').mt_rand()?>"></script>
    </head>
    <style>
        #kt_chat_modal{
            top: 0;
        }
        #kt_chat_modal .card-body .scroll{
            height: 250px !important;
        }
    </style>
    <script>
        var site = "<?php echo site_url('')?>";
    </script>
    <body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
        <div id="kt_header_mobile" class="header-mobile align-items-center header-mobile-fixed">
            <a href="<?php echo site_url('dashboard')?>">
                <img alt="Text Logo" src="<?php echo site_url('assets/project/logo.png?t=').mt_rand()?>" style="width: 125px; border-radius: 25px;" />
            </a>
            <div class="d-flex align-items-center">
                <button class="btn p-0 burger-icon burger-icon-left" id="kt_aside_mobile_toggle">
                    <span></span>
                </button>
                <button class="btn btn-hover-text-primary p-0 ml-2" id="kt_header_mobile_topbar_toggle">
                    <span class="svg-icon svg-icon-xl">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <polygon points="0 0 24 0 24 24 0 24" />
                                <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                            </g>
                        </svg>
                    </span>
                </button>
            </div>
        </div>
        <div class="d-flex flex-column flex-root">
            <div class="d-flex flex-row flex-column-fluid page">
                <div class="aside aside-left aside-fixed d-flex flex-column flex-row-auto" id="kt_aside">
                    <div class="brand flex-column-auto" id="kt_brand">
                        <a href="<?php echo site_url('dashboard')?>" class="brand-logo">
                            <img alt="Text Logo" src="<?php echo site_url('assets/project/logo.png?t=').mt_rand()?>" style="width: 150px; border-radius: 25px;" />
                        </a>
                        <button class="brand-toggle btn btn-sm px-0" id="kt_aside_toggle">
                            <span class="svg-icon svg-icon svg-icon-xl">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24" />
                                        <path d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z" fill="#000000" fill-rule="nonzero" transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999)" />
                                        <path d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999)" />
                                    </g>
                                </svg>
                            </span>
                        </button>
                    </div>
                    <div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">
                        <div id="kt_aside_menu" class="aside-menu my-4" data-menu-vertical="1" data-menu-scroll="1" data-menu-dropdown-timeout="500">
                            <ul class="menu-nav" style="padding: 0;">
                                <li class="menu-section" style="margin: 0;">
                                    <h4 class="menu-text">AKSES MENU</h4>
                                    <i class="menu-icon ki ki-bold-more-hor icon-md"></i>
                                </li>
                                <?php
                                    $id_group = $this->db->get_where('users_groups', array('user_id' => $this->ion_auth->user()->row()->id))->row()->group_id;
                                    $this->db->order_by('menu.urutan','asc');
                                    $this->db->select('menu.id_menu, menu.nama_menu, menu.url, menu.rel');
                                    $this->db->where('rel_group.id_group', $id_group);
                                    $this->db->where('menu.rel', '0');
                                    $this->db->where('menu.is_trash', '0');
                                    $this->db->where('rel_group.akses', '3');
                                    $this->db->from('rel_group');
                                    $this->db->join('menu', 'rel_group.id_menu = menu.id_menu');
                                    $menu = $this->db->get()->result();
                                    $arr_id = array();
                                    foreach ($menu as $r_menu) {
                                        array_push($arr_id, $r_menu->id_menu);
                                    }
                                    foreach ($menu as $row_menu) {
                                        $id_menu = $row_menu->id_menu;
                                        $this->db->order_by('menu.urutan','asc');
                                        $this->db->select('menu.id_menu, menu.nama_menu, menu.url, menu.rel');
                                        $this->db->where('rel_group.id_group', $id_group);
                                        $this->db->where('menu.rel', $id_menu);
                                        $this->db->where('menu.is_trash', '0');
                                        $this->db->where('rel_group.akses', '3');
                                        $this->db->from('rel_group');
                                        $this->db->join('menu', 'rel_group.id_menu = menu.id_menu');
                                        $sub_menu = $this->db->get()->result();
                                        if($sub_menu){
                                            echo '<li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">';
                                                echo '<a href="javascript:;" class="menu-link menu-toggle">';
                                                    echo '<span class="svg-icon menu-icon">';
                                                        echo '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">';
                                                            echo '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">';
                                                                echo '<rect x="0" y="0" width="24" height="24" />';
                                                                echo '<path d="M2.56066017,10.6819805 L4.68198052,8.56066017 C5.26776695,7.97487373 6.21751442,7.97487373 6.80330086,8.56066017 L8.9246212,10.6819805 C9.51040764,11.267767 9.51040764,12.2175144 8.9246212,12.8033009 L6.80330086,14.9246212 C6.21751442,15.5104076 5.26776695,15.5104076 4.68198052,14.9246212 L2.56066017,12.8033009 C1.97487373,12.2175144 1.97487373,11.267767 2.56066017,10.6819805 Z M14.5606602,10.6819805 L16.6819805,8.56066017 C17.267767,7.97487373 18.2175144,7.97487373 18.8033009,8.56066017 L20.9246212,10.6819805 C21.5104076,11.267767 21.5104076,12.2175144 20.9246212,12.8033009 L18.8033009,14.9246212 C18.2175144,15.5104076 17.267767,15.5104076 16.6819805,14.9246212 L14.5606602,12.8033009 C13.9748737,12.2175144 13.9748737,11.267767 14.5606602,10.6819805 Z" fill="#000000" opacity="0.3" />';
                                                                echo '<path d="M8.56066017,16.6819805 L10.6819805,14.5606602 C11.267767,13.9748737 12.2175144,13.9748737 12.8033009,14.5606602 L14.9246212,16.6819805 C15.5104076,17.267767 15.5104076,18.2175144 14.9246212,18.8033009 L12.8033009,20.9246212 C12.2175144,21.5104076 11.267767,21.5104076 10.6819805,20.9246212 L8.56066017,18.8033009 C7.97487373,18.2175144 7.97487373,17.267767 8.56066017,16.6819805 Z M8.56066017,4.68198052 L10.6819805,2.56066017 C11.267767,1.97487373 12.2175144,1.97487373 12.8033009,2.56066017 L14.9246212,4.68198052 C15.5104076,5.26776695 15.5104076,6.21751442 14.9246212,6.80330086 L12.8033009,8.9246212 C12.2175144,9.51040764 11.267767,9.51040764 10.6819805,8.9246212 L8.56066017,6.80330086 C7.97487373,6.21751442 7.97487373,5.26776695 8.56066017,4.68198052 Z" fill="#000000" />';
                                                            echo '</g>';
                                                        echo '</svg>';
                                                    echo '</span>';
                                                    echo '<span class="menu-text">' . $row_menu->nama_menu . '</span>';
                                                    echo '<i class="menu-arrow"></i>';
                                                echo '</a>';
                                                echo '<div class="menu-submenu">';
                                                    echo '<i class="menu-arrow"></i>';
                                                    echo '<ul class="menu-subnav">';
                                                        echo '<li class="menu-item menu-item-parent" aria-haspopup="true">';
                                                            echo '<span class="menu-link">';
                                                                echo '<span class="menu-text">' . $row_menu->nama_menu . '</span>';
                                                            echo '</span>';
                                                        echo '</li>';
                                                        foreach ($sub_menu as $row_submenu) {
                                                            echo '<li class="menu-item" aria-haspopup="true">';
                                                                echo '<a href="' . site_url($row_submenu->url) . '" class="menu-link">';
                                                                    echo '<i class="menu-bullet menu-bullet-dot">';
                                                                        echo '<span></span>';
                                                                    echo '</i>';
                                                                    echo '<span class="menu-text">' . $row_submenu->nama_menu . '</span>';
                                                                echo '</a>';
                                                            echo '</li>';
                                                        }
                                                    echo '</ul>';
                                                echo '</div>';
                                            echo '</li>';
                                        }else{
                                            echo '<li class="menu-item" aria-haspopup="true">';
                                                echo '<a href="' . site_url($row_menu->url) . '" class="menu-link">';
                                                    echo '<span class="svg-icon menu-icon">';
                                                        echo '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">';
                                                            echo '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">';
                                                                echo '<rect x="0" y="0" width="24" height="24" />';
                                                                echo '<rect fill="#000000" opacity="0.3" x="4" y="5" width="16" height="6" rx="1.5" />';
                                                                echo '<rect fill="#000000" x="4" y="13" width="16" height="6" rx="1.5" />';
                                                            echo '</g>';
                                                        echo '</svg>';
                                                    echo '</span>';
                                                    echo '<span class="menu-text">' . $row_menu->nama_menu . '</span>';
                                                echo '</a>';
                                            echo '</li>';
                                        }
                                    }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
                    <div id="kt_header" class="header header-fixed">
                        <div class="container-fluid d-flex align-items-stretch justify-content-between">
                            <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
                            </div>
                            <div class="topbar">
                                <?php if($id_group == '1'){ ?>
                                <div class="topbar-item"></div>
                                <div class="topbar-item">
                                    <div class="btn btn-icon btn-clean btn-lg mr-1" id="kt_quick_panel_toggle">
                                        <span class="svg-icon svg-icon-xl svg-icon-primary">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <rect fill="#000000" x="4" y="4" width="7" height="7" rx="1.5" />
                                                    <path d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z" fill="#000000" opacity="0.3" />
                                                </g>
                                            </svg>
                                        </span>
                                    </div>
                                </div>
                                <?php }else{ ?>
                                <div class="topbar-item"></div>
                                <?php } ?>
                                <div class="topbar-item">
                                    <div class="btn btn-icon btn-icon-mobile w-auto btn-clean d-flex align-items-center btn-lg px-2" id="kt_quick_user_toggle">
                                        <span class="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3"><?php echo $this->ion_auth->user()->row()->username?></span>
                                        <span class="symbol symbol-lg-35 symbol-25 symbol-light-success">
                                            <span class="symbol-label font-size-h5 font-weight-bold"><?php echo substr($this->ion_auth->user()->row()->username,0,1)?></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>