<!DOCTYPE html>
<html lang="en">
<head>
    <title>Tama - Absensi Online</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
    <meta name="description" content="Tama Aplikasi Absensi Online Untuk Organisasi, PT, Perusahaan, Lembaga, Dan Instansi Pemerintahan, Dapat Dilakukan Dimana Saja Dan Kapan Saja">
    <meta name="keywords" content="Tama,Absensi,Online,Kepegawaian,Sistem,Perusahaan,Organisasi,Aplikasi">
    <meta name="googlebot-news" content="index,follow">
    <meta name="googlebot" content="index,follow">
    <meta name="author" content="Antahassy Wibawa">
    <meta name="robots" content="index,follow">
    <meta name="language" content="id">
    <meta name="Classification" content="Technology">
    <meta name="geo.country" content="Indonesia">
    <meta name="geo.placename" content="Indonesia"> 
    <meta name="geo.position" content="-6.5899176; 106.8230479">
    <!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->
    <meta http-equiv="content-language" content="In-Id">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Copyright" content="Tama - Absensi Online">
    <meta property="og:title" content="Tama - Absensi Online">
    <meta property="og:url" content="tama.id">
    <meta property="og:type" content="Application">
    <meta property="og:site_name" content="Tama - Absensi Online">
    <meta itemprop="name" content="Tama - Absensi Online">

    <link rel="shortcut icon" href="<?php echo site_url('assets/tama.png')?>">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo site_url('assets/tama.png')?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo site_url('assets/tama.png')?>">

    <link href="https://fonts.googleapis.com/css?family=Poppins:400,600|Roboto:400,400i,500" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo site_url('assets/appru/css/linearicons.css')?>">
    <link rel="stylesheet" href="<?php echo site_url('assets/appru/css/font-awesome.min.css')?>">
    <link rel="stylesheet" href="<?php echo site_url('assets/appru/css/bootstrap.css')?>">
    <link rel="stylesheet" href="<?php echo site_url('assets/appru/css/magnific-popup.css')?>">
    <link rel="stylesheet" href="<?php echo site_url('assets/appru/css/nice-select.css')?>">
    <link rel="stylesheet" href="<?php echo site_url('assets/appru/css/hexagons.min.css')?>">
    <link rel="stylesheet" href="<?php echo site_url('assets/appru/css/owl.carousel.css')?>">
    <link rel="stylesheet" href="<?php echo site_url('assets/appru/css/main.css')?>">
    <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/antahassy.css')?>">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
     crossorigin="anonymous"></script>
     <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>

    <script src="<?php echo site_url('assets/appru/js/vendor/jquery-2.2.4.min.js')?>"></script>
    <script src="<?php echo site_url('assets/appru/js/tilt.jquery.min.js')?>"></script>
    <script src="<?php echo site_url('assets/appru/js/vendor/bootstrap.min.js')?>"></script>
    <script src="<?php echo site_url('assets/appru/js/easing.min.js')?>"></script>
    <script src="<?php echo site_url('assets/appru/js/hoverIntent.js')?>"></script>
    <script src="<?php echo site_url('assets/appru/js/superfish.min.js')?>"></script>
    <script src="<?php echo site_url('assets/appru/js/jquery.ajaxchimp.min.js')?>"></script>
    <script src="<?php echo site_url('assets/appru/js/jquery.magnific-popup.min.js')?>"></script>
    <script src="<?php echo site_url('assets/appru/js/owl.carousel.min.js')?>"></script>
    <script src="<?php echo site_url('assets/appru/js/owl-carousel-thumb.min.js')?>"></script>
    <script src="<?php echo site_url('assets/appru/js/hexagons.min.js')?>"></script>
    <script src="<?php echo site_url('assets/appru/js/jquery.nice-select.min.js')?>"></script>
    <script src="<?php echo site_url('assets/appru/js/waypoints.min.js')?>"></script>
    <script src="<?php echo site_url('assets/appru/js/mail-script.js')?>"></script>
    <script src="<?php echo site_url('assets/appru/js/main.js')?>"></script>

    <!-- <link rel="stylesheet" id="css-main" href="{{ asset('css/dashmix.min.css?t=').mt_rand() }}">
    <link rel="stylesheet" id="css-theme" href="{{ asset('css/xmodern.min.css?t=').mt_rand() }}">
    <link rel="stylesheet" href="{{ asset('css/antahassy.css?t=').mt_rand() }}">
    <link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap4.css?t=').mt_rand() }}">
    <link rel="stylesheet" href="{{ asset('css/buttons.bootstrap4.min.css?t=').mt_rand() }}">
    <link rel="stylesheet" href="{{ asset('css/dataTables.buttons.min.css?t=').mt_rand() }}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css?t=').mt_rand() }}">
    <link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css?t=').mt_rand() }}">

    <script src="{{ asset('js/dashmix.core.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('js/dashmix.app.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('js/jquery.dataTables.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap4.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('js/dataTables.buttons.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('js/buttons.print.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('js/buttons.html5.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('js/buttons.flash.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('js/buttons.colVis.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('js/be_tables_datatables.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('js/zip.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('js/pdfmake.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('js/vfs_fonts.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('js/jquery-ui.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('js/sweetalert2.all.min.js?t=').mt_rand() }}"></script> -->
</head>
<style>
    table tr th, table tr td{
        padding: 2.5px 7.5px;
    }
    #mobile-body-overly{
        background: rgba(0, 0, 0, 0.5);
    }
    #mobile-nav{
        width: 200px;
        background: rgba(0, 0, 0, 0.75);
        transition: 0.5s;
    }
    #mobile-nav-toggle{
        top: 38px;
    }
    #index_login_btn{
        background-color: rgba(87,2,168,255);
        color: #fff;
    }
    #login_btn{
        margin-left: 100px;
        background-color: rgba(87,2,168,255);
        padding: 0 5px;
        border-radius: 5px;
    }
    @media screen and (max-width: 960px){
        #login_btn{
            margin-left: 0;
            background-color: transparent;
            padding: 0;
            border-radius: 0;
        }
    }
</style>
<script>
    var site = "<?php echo site_url()?>";
</script>
<body>
    <header id="header" style="padding: 0;">
        <div class="container">
            <div class="row align-items-center justify-content-between d-flex"> 
                <div id="logo">
                    <a href="<?php echo site_url('home')?>"><img src="<?php echo site_url('assets/tama.png')?>" alt="Logo Tama" title="Tama - Absensi Online" style="width: 125px !important; height: 100px !important; max-height: 100px !important;" /></a>
                </div>
                <nav id="nav-menu-container">
                    <ul class="nav-menu">
                        <li class="m_beranda"><a href="#">Beranda</a></li>
                        <li class="m_fitur"><a href="#">Fitur</a></li>
                        <li class="m_biaya"><a href="#">Biaya</a></li>
                        <li class="m_kontak"><a href="#">Kontak</a></li>
                        <li id="login_btn"><a href="http://mpt-precense.qelopak.com/" target="_blank">Login</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>