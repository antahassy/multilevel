function toggle_tambah(source) {
    checkboxes = document.getElementsByName('tambah[]');
    for(var i=0, n=checkboxes.length;i<n;i++) {
        checkboxes[i].checked = source.checked;
    }
}
function toggle_edit(source) {
    checkboxes = document.getElementsByName('edit[]');
    for(var i=0, n=checkboxes.length;i<n;i++) {
        checkboxes[i].checked = source.checked;
    }
}
function toggle_lihat(source) {
    checkboxes = document.getElementsByName('lihat[]');
    for(var i=0, n=checkboxes.length;i<n;i++) {
        checkboxes[i].checked = source.checked;
    }
}
function toggle_hapus(source) {
    checkboxes = document.getElementsByName('hapus[]');
    for(var i=0, n=checkboxes.length;i<n;i++) {
        checkboxes[i].checked = source.checked;
    }
}
function save(){
    swal({
        showConfirmButton   : false,
        allowOutsideClick   : false,
        allowEscapeKey      : false,
        background          : 'transparent',
        onOpen  : function(){
            swal.showLoading();
            setTimeout(function(){
                $.ajax({
                    type        : 'ajax',
                    method      : 'post',
                    data        : $('#form').serialize(),
                    url         : site + 'group/ajax_akses',
                    dataType    : "json",
                    async       : true,
                    success: function(data){
                        if(data.status){
                            swal({
                                background  : 'transparent',
                                html        : '<pre>' + level_akses + ' berhasil diperbaharui</pre>',
                                type        : "success"
                            }).then(function(){
                                location.reload(true);
                            });
                        }else{
                            for (var i = 0; i < data.inputerror.length; i++){
                                $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error');
                                $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                            }
                        }
                    },
                    error: function (){
                        swal({
                            background  : 'transparent',
                            html        : '<pre>Koneksi terputus' + '<br>' + 
                                          'Cobalah beberapa saat lagi</pre>',
                            type        : "warning"
                        });
                    }
                });
            },500);
        }
    });
}