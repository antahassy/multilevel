$(document).ready(function(){
    var nama_bulan = new Array();
    nama_bulan[1] = 'Januari';
    nama_bulan[2] = 'Februari';
    nama_bulan[3] = 'Maret';
    nama_bulan[4] = 'April';
    nama_bulan[5] = 'Mei';
    nama_bulan[6] = 'Juni';
    nama_bulan[7] = 'Juli';
    nama_bulan[8] = 'Agustus'; 
    nama_bulan[9] = 'September'; 
    nama_bulan[10] = 'Oktober';
    nama_bulan[11] = 'November'; 
    nama_bulan[12] = 'Desember';
    var table;
    swal({
        showConfirmButton   : false,
        allowOutsideClick   : false,
        allowEscapeKey      : false,
        background          : 'transparent',
        onOpen  : function(){
            swal.showLoading();
            setTimeout(function(){
                s_side_data_table();
            },500);
        }
    });
    function s_side_data_table(){
        table = $('#table_data').DataTable({ 
            processing          : true, 
            destroy             : true,
            serverSide          : true, 
            scrollX             : true,
            scrollCollapse      : true,
            fixedColumns        : true, 
            lengthMenu          : [[10, 25, 50, 100, 250, 500, -1], [10, 25, 50, 100, 250, 500, 'Semua']],
            initComplete: function(){
                swal.close();
                main();
            },
            ajax            : {
                url         : site + 'group/s_side_data',
                method      : 'post'
            },
            columnDefs: [{ 
                targets   : [0], 
                orderable : false 
            }]
        });
    }
    function main(){
        var modal_form;
        $('#modal_form').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_form = true;
        });
        $('#modal_form').on('hide.bs.modal', function(){
            if(modal_form){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_form = false;
                setTimeout(function(){
                    $('#modal_form').modal('hide');
                },350);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        $('#btn_add').on('click', function(){
            $('#form_data')[0].reset();
            $('#modal_form').find('.modal-title').text('Tambah');
            $('#form_data').attr('action', site + 'group/save');
            $('#btn_process').text('Simpan');
            $('#modal_form').modal('show');
            data_process();
        });
        $('#table_data').on('click', '#btn_edit', function(){
            var id = $(this).attr('data');
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type           : 'ajax',
                            method         : 'post',
                            url            : site + 'group/edit',
                            data           : {id : id},
                            async          : true,
                            dataType       : 'json',
                            success        : function(data){
                                $('#form_data')[0].reset();
                                $('#id_data').val(data.id);
                                $('#nama').val(data.name);
                                $('#deskripsi').val(data.description);
                                $('#modal_form').find('.modal-title').text('Edit');
                                $('#form_data').attr('action', site + 'group/update');
                                $('#btn_process').text('Update');
                                swal.close();
                                $('#modal_form').modal('show');
                                data_process();
                            },
                            error     : function(){
                                swal({
                                    html        : '<pre>Koneksi terputus' + '<br>' +
                                                  'Cobalah beberapa saat lagi</pre>',
                                    type        : "warning",
                                    background  : 'transparent'
                                });
                            }
                        });
                    },500);
                }
            });
        });
        $('#table_data').on('click', '#btn_delete', function(){
            var id = $(this).attr('data');
            swal({
                html                : '<pre>Beberapa user akan kehilangan' + '<br>' + 
                                      'Level & akses menu' + '<br>' + 
                                      'Hapus data ini ?</pre>',
                type                : "question",
                background          : 'transparent',
                showCancelButton    : true,
                cancelButtonText    : 'Tidak',
                confirmButtonText   : 'Ya'
            }).then((result) => {
                if(result.value){
                    swal({
                        showConfirmButton   : false,
                        allowOutsideClick   : false,
                        allowEscapeKey      : false,
                        background          : 'transparent',
                        onOpen  : function(){
                            swal.showLoading();
                            setTimeout(function(){
                                $.ajax({
                                    type        : 'ajax',
                                    method      : 'post',
                                    data        : {id : id},
                                    url         : site + 'group/delete',
                                    dataType    : "json",
                                    async       : true,
                                    success: function(data){
                                        swal.close();
                                        setTimeout(function(){
                                            table.ajax.reload();
                                        },500);
                                    },
                                    error: function (){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Koneksi terputus' + '<br>' + 
                                                          'Cobalah beberapa saat lagi</pre>',
                                            type        : "warning"
                                        });
                                    }
                                });
                            },500);
                        }
                    });
                }
            });
        });
    }
    function data_process(){
        $('#btn_process').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var url             = $('#form_data').attr('action');
            var form_data       = $('#form_data')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('#nama').val()){
                 errormessage += 'Nama dibutuhkan \n';
            }
            if(! $('#deskripsi').val()){
                 errormessage += 'Deskripsi dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type           : 'ajax',
                                method         : 'post',
                                url            : url,
                                data           : form_data,
                                async          : true,
                                processData    : false,
                                contentType    : false,
                                cache          : false,
                                dataType       : 'json',
                                success        : function(response){
                                    if(response.success){
                                        $('#modal_form').modal('hide');
                                        $('#form_data')[0].reset();
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Data berhasil ' + response.type + '</pre>',
                                            type        : "success"
                                        }).then(function(){
                                            setTimeout(function(){
                                                table.ajax.reload();
                                            },500);
                                        });
                                    }
                                },
                                error   : function(){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Koneksi terputus' + '<br>' + 
                                                      'Cobalah beberapa saat lagi</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });
            }
            return false;
        });
    }
});