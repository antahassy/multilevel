$(document).ready(function(){
    $.get('https://jsonip.com', function(address){
        $('#ip_address').val(address.ip);
    });
    login_user();
    function login_user(){
        $('#form_login').on('submit', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var errormessage = '';
            if(! $('#username').val()){
                errormessage += 'Username dibutuhkan \n';
            }
            if(! $('#password').val()){
                errormessage += 'Password dibutuhkan \n';
            }
            if(! $('#captcha').val()){
                errormessage += 'Captcha dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    html          : '<pre>' + errormessage + '</pre>',
                    background  : 'transparent'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type           : 'ajax',
                                method         : 'post',
                                url            :  site + 'main/login_process',
                                data           : $('#form_login').serialize(),
                                async          : true,
                                dataType       : 'json',
                                success        : function(response){
                                    if(response.match){
                                        swal({
                                            html        : '<pre>Password tidak cocok</pre>',
                                            background  : 'transparent'
                                        });
                                    }else if(response.success){
                                        location.href = site + 'dashboard';
                                    }else if(response.username){
                                        swal({
                                            html        : '<pre>Akun tidak terdaftar</pre>',
                                            background  : 'transparent'
                                        });
                                    }else if(response.inactive){
                                        swal({
                                            html        : '<pre>Akun dinonaktifkan' + '<br>' + 
                                                          'Harap hubungi administrator</pre>',
                                            background  : 'transparent'
                                        });
                                    }else if(response.captcha_false){
                                        swal({
                                            html        : '<pre>Captcha tidak cocok</pre>',
                                            background  : 'transparent'
                                        }).then(function(){
                                            location.reload(true);
                                        });
                                    }
                                },
                                error     : function(){
                                    swal({
                                        html        : '<pre>Koneksi terputus' + '<br>' +
                                                      'Cobalah beberapa saat lagi</pre>',
                                        type        : "warning",
                                        background  : 'transparent'
                                    });
                                }
                            });
                        },500);
                    }
                });
            }
        });
    }
});