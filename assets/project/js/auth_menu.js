$(document).ready(function(){
    var nama_bulan = new Array();
    nama_bulan[1] = 'Januari';
    nama_bulan[2] = 'Februari';
    nama_bulan[3] = 'Maret';
    nama_bulan[4] = 'April';
    nama_bulan[5] = 'Mei';
    nama_bulan[6] = 'Juni';
    nama_bulan[7] = 'Juli';
    nama_bulan[8] = 'Agustus'; 
    nama_bulan[9] = 'September'; 
    nama_bulan[10] = 'Oktober';
    nama_bulan[11] = 'November'; 
    nama_bulan[12] = 'Desember';
    var table;
    swal({
        showConfirmButton   : false,
        allowOutsideClick   : false,
        allowEscapeKey      : false,
        background          : 'transparent',
        onOpen  : function(){
            swal.showLoading();
            setTimeout(function(){
                s_side_data_table();
            },500);
        }
    });
    function rel_menu(rel_val){
        $.ajax({
            type        : 'ajax',
            method      : 'get',
            url         : site + 'menu/rel_menu',
            dataType    : "json",
            async       : true,
            success: function(rel_data){
                console.log(rel_data)
                if(rel_data.empty){
                    swal({
                        background  : 'transparent',
                        html        : '<pre>Menu belum tersedia</pre>',
                        type        : "warning"
                    });
                }else{
                    var s_rel = '<option value="">Pilih Rel</option>';
                    for(i = 0; i < rel_data.length; i ++){
                        s_rel += '<option value="' + rel_data[i].id + '">' + rel_data[i].menu + '</option>';
                    }
                    $('#s_rel').html(s_rel);
                    if(rel_val != ''){
                        $('#s_rel').val(rel_val);
                    }else{
                        $('#s_rel').val('');
                    }
                    swal.close();
                }
                $('#modal_form').modal('show');
                data_process();
            },
            error: function (){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Koneksi terputus' + '<br>' + 
                                  'Cobalah beberapa saat lagi</pre>',
                    type        : "warning"
                });
            }
        });
    }
    function s_side_data_table(){
        table = $('#table_data').DataTable({ 
            processing          : true, 
            destroy             : true,
            serverSide          : true, 
            scrollX             : true,
            scrollCollapse      : true,
            fixedColumns        : true, 
            ordering            : false, 
            lengthMenu          : [[10, 25, 50, 100, 250, 500, -1], [10, 25, 50, 100, 250, 500, 'Semua']],
            initComplete: function(){
                swal.close();
                main();
            },
            ajax            : {
                url         : site + 'menu/s_side_data',
                method      : 'post'
            },
            columnDefs: [{ 
                targets   : [0]
            }]
        });
    }
    function main(){
        var modal_form;
        $('#modal_form').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_form = true;
        });
        $('#modal_form').on('hide.bs.modal', function(){
            if(modal_form){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_form = false;
                setTimeout(function(){
                    $('#modal_form').modal('hide');
                },350);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        $('#btn_add').on('click', function(){
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $('#form_data')[0].reset();
                        $('#modal_form').find('.modal-title').text('Tambah');
                        $('#form_data').attr('action', site + 'menu/save');
                        $('#btn_process').text('Simpan');
                        var rel_val = '';
                        rel_menu(rel_val);
                    },500);
                }
            });
        });
        $('#table_data').on('click', '#btn_edit', function(){
            var id = $(this).attr('data');
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type           : 'ajax',
                            method         : 'post',
                            url            : site + 'menu/edit',
                            data           : {id : id},
                            async          : true,
                            dataType       : 'json',
                            success        : function(data){
                                $('#form_data')[0].reset();
                                $('#id_data').val(data.id_menu);
                                $('#nama').val(data.nama_menu);
                                $('#url').val(data.url);
                                $('#urutan').val(data.urutan);
                                $('#modal_form').find('.modal-title').text('Edit');
                                $('#form_data').attr('action', site + 'menu/update');
                                $('#btn_process').text('Update');
                                var rel_val = '';
                                if(data.rel != '0'){
                                    rel_val = data.rel;
                                }
                                rel_menu(rel_val);
                            },
                            error     : function(){
                                swal({
                                    html        : '<pre>Koneksi terputus' + '<br>' +
                                                  'Cobalah beberapa saat lagi</pre>',
                                    type        : "warning",
                                    background  : 'transparent'
                                });
                            }
                        });
                    },500);
                }
            });
        });
        $('#table_data').on('click', '#btn_delete', function(){
            var id = $(this).attr('data');
            swal({
                html                : '<pre>Beberapa user akan kehilangan' + '<br>' + 
                                      'Akses menu ini' + '<br>' + 
                                      'Hapus data ini ?</pre>',
                type                : "question",
                background          : 'transparent',
                showCancelButton    : true,
                cancelButtonText    : 'Tidak',
                confirmButtonText   : 'Ya'
            }).then((result) => {
                if(result.value){
                    swal({
                        showConfirmButton   : false,
                        allowOutsideClick   : false,
                        allowEscapeKey      : false,
                        background          : 'transparent',
                        onOpen  : function(){
                            swal.showLoading();
                            setTimeout(function(){
                                $.ajax({
                                    type        : 'ajax',
                                    method      : 'post',
                                    data        : {id : id},
                                    url         : site + 'menu/delete',
                                    dataType    : "json",
                                    async       : true,
                                    success: function(data){
                                        swal.close();
                                        setTimeout(function(){
                                            table.ajax.reload();
                                        },500);
                                    },
                                    error: function (){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Koneksi terputus' + '<br>' + 
                                                          'Cobalah beberapa saat lagi</pre>',
                                            type        : "warning"
                                        });
                                    }
                                });
                            },500);
                        }
                    });
                }
            });
        });
    }
    function data_process(){
        $('#btn_process').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var url             = $('#form_data').attr('action');
            var form_data       = $('#form_data')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('#nama').val()){
                 errormessage += 'Nama menu dibutuhkan \n';
            }
            if(! $('#url').val()){
                 errormessage += 'Url menu dibutuhkan \n';
            }
            if(! $('#urutan').val()){
                 errormessage += 'Urutan menu dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type           : 'ajax',
                                method         : 'post',
                                url            : url,
                                data           : form_data,
                                async          : true,
                                processData    : false,
                                contentType    : false,
                                cache          : false,
                                dataType       : 'json',
                                success        : function(response){
                                    if(response.success){
                                        $('#modal_form').modal('hide');
                                        $('#form_data')[0].reset();
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Data berhasil ' + response.type + '</pre>',
                                            type        : "success"
                                        }).then(function(){
                                            setTimeout(function(){
                                                table.ajax.reload();
                                            },500);
                                        });
                                    }
                                },
                                error   : function(){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Koneksi terputus' + '<br>' + 
                                                      'Cobalah beberapa saat lagi</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });
            }
            return false;
        });
    }
});