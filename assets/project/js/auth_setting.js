$(document).ready(function(){
    update_setting();
    display_image();

    function update_setting(){
        $('#btn_process').on('click', function(){
            var form_data       = $('#form')[0];
            form_data           = new FormData(form_data);
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type            : 'ajax',
                            method          : 'post',
                            data            : form_data,
                            url             : site + 'setting/update',
                            dataType        : "json",
                            processData     : false,
                            contentType     : false,
                            cache           : false,
                            async           : true,
                            success: function(response){
                                if(response.success){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Pengaturan sistem berhasil diperbaharui</pre>',
                                        type        : "success"
                                    }).then(function(){
                                        location.reload(true);
                                    });
                                }else{
                                    for (var i = 0; i < response.inputerror.length; i++){
                                        $('[name="'+response.inputerror[i]+'"]').parent().addClass('has-error');
                                        $('[name="'+response.inputerror[i]+'"]').next().text(response.error_string[i]);
                                    }
                                }
                            },
                            error: function (){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Koneksi terputus' + '<br>' + 
                                                  'Cobalah beberapa saat lagi</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
    }
    function display_image(){
        function preview(image){
            if(image.files && image.files[0]){
                var reader      = new FileReader();
                reader.onload   = function(event){
                    $('#preview_items').attr('src', event.target.result);
                    $('#preview_items').attr('title', image.files[0].name);
                    $('#delete_preview_items').css('display','block');
                }
                reader.readAsDataURL(image.files[0]);
            }
        }
        $("#image").on('change', function(){
            preview(this);
            var names = $(this).val();
            var file_names = names.replace(/^.*\\/, "");
        });
        $('#delete_preview_items').on('click', function(){
            $('#delete_preview_items').css('display','none');
            $('#preview_items').attr('src', '');
            $('#image, #get_image').val('');
        });
    }
});