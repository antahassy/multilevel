$(document).ready(function(){
    var all_chart;
    var sum = (index, value) => index + value;
    function unique(list) {
        var result = [];
        $.each(list, function(i, e) {
            if($.inArray(e, result) == -1){
                result.push(e);
            }
        });
        return result;
    }
    var nama_bulan = new Array();
    nama_bulan[1] = 'Januari';
    nama_bulan[2] = 'Februari';
    nama_bulan[3] = 'Maret';
    nama_bulan[4] = 'April';
    nama_bulan[5] = 'Mei';
    nama_bulan[6] = 'Juni';
    nama_bulan[7] = 'Juli';
    nama_bulan[8] = 'Agustus'; 
    nama_bulan[9] = 'September'; 
    nama_bulan[10] = 'Oktober';
    nama_bulan[11] = 'November'; 
    nama_bulan[12] = 'Desember';
    var table;
    swal({
        showConfirmButton   : false,
        allowOutsideClick   : false,
        allowEscapeKey      : false,
        background          : 'transparent',
        onOpen  : function(){
            swal.showLoading();
            setTimeout(function(){
                var periode_val = '1';
                graph_data(periode_val);
            },500);
        }
    });
    function graph_data(periode_val){
        $.ajax({
            type        : 'ajax',
            method      : 'post',
            url         : site + 'bonus_pairing/data_grafik',
            data        : {
                id_user : $('#s_member_hidden').val(),
                periode : periode_val
            },
            async       : true,
            dataType    : 'json',
            success     : function(data){
                var x_param = [];
                if(data.pairing_param.length != 0){
                    for(i = 0; i < data.pairing_param.length; i ++){
                        x_param.push(data.pairing_param[i]);
                    }
                }
                x_param = unique(x_param);
                var all_width = x_param.length * 75;
                $('#canvas_area_all').attr('style', 'min-width: ' + all_width + 'px');
                x_param.sort(function(a, b) {
                    if (new Date(a) < new Date(b)) {
                         return -1;
                    }
                    if (new Date(a) > new Date(b)) {
                         return 1;
                    }
                });
                var arr_bonus = ['Bonus Pairing'];
                var arr_bonus_data = [];
                var y_param = [];
                for(i = 0; i < x_param.length; i ++){
                    var bonus_data = 0;
                    for(z = 0; z < data.pairing_data.length; z ++){
                        if(x_param[i] == data.pairing_data[z].parameter){
                            bonus_data = bonus_data + data.pairing_data[z].data
                        }
                    }
                    arr_bonus_data.push(bonus_data);
                    //////////////////////////
                    var s_param = x_param[i].split('-');
                    if(s_param.length == 1){
                        x_param[i] = s_param[0];
                    }
                    if(s_param.length == 2){
                        x_param[i] = nama_bulan[Number(s_param[1])] + '/' + s_param[0];  
                    }
                    if(s_param.length == 3){
                        x_param[i] = s_param[2] + '/' + nama_bulan[Number(s_param[1])] + '/' + s_param[0];
                    }
                }
                y_param.push(arr_bonus_data);
                line_chart_process(x_param, y_param, arr_bonus);
            },
            error       : function(){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Koneksi terputus' + '<br>' + 
                                  'Cobalah beberapa saat lagi</pre>',
                    type        : "warning"
                });
            }
        });
    }
    function line_chart_process(x_param, y_param, arr_bonus){
        if(typeof all_chart != 'undefined'){
            all_chart.destroy();
        }
        var total_all = [];
        var max_all = [];
        for(i = 0; i < y_param.length; i ++){
            var each_total = y_param[i].reduce(sum);
            total_all.push(Number(each_total));
            for(z = 0; z < y_param[i].length; z ++){
                max_all.push(Number(y_param[i][z]));
            }
        }
        max_all = Math.max(...max_all);
        var all_graphic = $('#line_chart_all')[0].getContext('2d');
        var arr_datasets = [], colors = [
            '#00BFFF', 
            '#FA8072', 
            '#7CFC00', 
            '#F0E68C', 
            '#EE82EE', 
            ////////////////////
            '#1E90FF', 
            '#DC143C', 
            '#3CB371', 
            '#FFD700', 
            '#FF00FF', 
            ///////////////////
            '#0000FF', 
            '#FF0000', 
            '#008000', 
            '#FF8C00', 
            '#8A2BE2', 
            ///////////////////
            '#000080', 
            '#800000', 
            '#556B2F', 
            '#D2691E', 
            '#4B0082', 
        ];
        for(i = 0; ((i < arr_bonus.length) && (i < y_param.length)); i ++){
            d_object = {
                label               : arr_bonus[i],
                data                : y_param[i],
                backgroundColor     : colors[i],
                borderColor         : colors[i],
                borderWidth         : 3,
                fill                : false
            }
            arr_datasets.push(d_object);
        }
        var all_data = {
            labels    : x_param,
            datasets  : arr_datasets
        };
        var all_option = {
            legend :{
                display : false
            },
            events: ['mousemove'],
            onHover: (event, element) => {
                event.target.style.cursor = element[0] ? 'pointer' : 'default';
            },
            plugins   : {
                datalabels: {
                    anchor: 'end',
                    align: 'top',
                    font: {
                        weight: '600',
                        size: 14
                    },
                    formatter: (value) => {
                        return Number(value).toLocaleString('de');
                    }
                }
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            tooltips  : {
                displayColors       : false,
                backgroundColor     : 'rgba(0,0,0,0.75)',
                callbacks           : {
                    label          : function(tooltips, data){
                        if(tooltips.datasetIndex === 0){
                            var val = data['datasets'][0]['data'][tooltips['index']];
                            var percentage = (val / total_all[0]) * 100;
                            percentage = percentage.toFixed(2);
                            return val + ' (' + percentage +  '%)';
                        }
                    },
                    afterLabel: function(tooltips, data){
                        if(tooltips.datasetIndex === 0){
                            var val = data['datasets'][0]['data'][tooltips['index']];
                            return 'Dari total ' + arr_bonus[0];
                        }
                    }
                }
            },
            scales    : {
                yAxes     : [{
                    ticks     : {
                        beginAtZero    :true,
                        stepSize       : (max_all / 5),
                        max            : max_all,
                        fontColor      : 'black',
                        fontSize       : 14,
                        callback       : function(value){
                             return value;
                        }
                    },
                    scaleLabel : {
                        display        : true,
                        labelString    : '',
                        fontColor      : 'black',
                        fontSize       : 14,
                        fontStyle      : 'bold'
                    },
                    gridLines : {
                        color          : 'rgba(0,0,0,0.25)'
                    }
                }],
                xAxes     : [{
                    ticks     : {
                        fontColor      : 'black',
                        fontSize       : 14
                    },
                    scaleLabel: {
                        display        : true,
                        labelString    : '',
                        fontColor      : 'black',
                        fontSize       : 14,
                        fontStyle      : 'bold'
                    },
                    gridLines : {
                        color          : 'rgba(0,0,0,0.25)'
                    }
                }]
            }
        };
        all_chart = new Chart(all_graphic, {
            type: 'line',
            data: all_data,
            options: all_option
        });
        $('#line_chart_all').attr('style', 'height: 425px');
        if(typeof table == 'undefined'){
            s_side_data_table();
        }else{
            swal.close();
        }
    }
    function s_side_data_table(){
        table = $('#table_data').DataTable({ 
            processing          : true, 
            destroy             : true,
            serverSide          : true, 
            scrollX             : true,
            scrollCollapse      : true,
            fixedColumns        : true, 
            lengthMenu          : [[10, 25, 50, 100, 250, 500, -1], [10, 25, 50, 100, 250, 500, 'Semua']],
            initComplete: function(){
            	swal.close();
                main();
            },
            ajax            : {
                url         : site + 'bonus_pairing/s_side_data',
                method      : 'post'
            },
            columnDefs: [{ 
                targets   : [0], 
                orderable : false 
            }]
        });
    }
    function main(){
        $('#s_periode').on('change', function(){
            var periode_val = $(this).val();
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        graph_data(periode_val);
                    },500);
                }
            });
        });
        $('#s_member').autocomplete({
            delay   : 1500,
            source  : site + 'bonus_pairing/autocomplete_username',
            select  : function(event, ui){
                var value = ui.item.value;
                $.ajax({
                    type        : 'ajax',
                    method      : 'post',
                    url         : site + 'bonus_pairing/username_complete',
                    data        : {username : value},
                    async       : true,
                    dataType    : 'json',
                    success     : function(data){
                        $('#s_member_hidden').val(data.id);
                        var periode_val = $('#s_periode').val();
                        if(periode_val != ''){
                            swal({
                                showConfirmButton   : false,
                                allowOutsideClick   : false,
                                allowEscapeKey      : false,
                                background          : 'transparent',
                                onOpen  : function(){
                                    swal.showLoading();
                                    setTimeout(function(){
                                        graph_data(periode_val);
                                    },500);
                                }
                            });
                        }
                    },
                    error       : function(){
                        swal({
                            background  : 'transparent',
                            html        : '<pre>Koneksi terputus' + '<br>' + 
                                          'Cobalah beberapa saat lagi</pre>',
                            type        : "warning"
                        });
                    }
                });
            }
        });
        $('#btn_process').on('click', function(){
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type           : 'ajax',
                            method         : 'get',
                            url            : site + 'process/pairing',
                            async          : true,
                            dataType       : 'json',
                            success        : function(response){
                                if(response.success){
                                    swal.close();
                                    setTimeout(function(){
                                        table.ajax.reload();
                                    },500);
                                }
                            },
                            error     : function(){
                                swal({
                                    html        : '<pre>Koneksi terputus' + '<br>' +
                                                  'Cobalah beberapa saat lagi</pre>',
                                    type        : "warning",
                                    background  : 'transparent'
                                });
                            }
                        });
                    },500);
                }
            });
        });
    }
});