$(document).ready(function(){
	var tree = '';
	var list_node = [];
	var tree_node = [];
	var left_node = [];
	var right_node = [];
	class BST{
		constructor(){
			this.root = null;
		}
		insert(up, value, names, pkt){
			const new_node = new Node(up, value, names, pkt);
			if(! this.root){
				this.root = new_node;
				tree_node.push({
					node	: '',
					up		: up,
					down 	: [value],
					name 	: names,
					paket	: pkt
				});
				list_node.push(value);
				return this;
			}else{
				list_node.push(value);
				let current = this.root;
				let center = Number(this.root.value);
				while(true){
					if(value < current.value){
						if(! current.left){
							current.left = new_node;
							if(value < center){
								if(left_node.length == 0 && value < center){
									left_node.push(new_node);
								}else{
									left_node.filter(function(element){
										if(element.value !== value && value < center){
											left_node.push(new_node);
										}
									});
								}
							}else{
								if(right_node.length == 0 && value > center){
									right_node.push(new_node);
								}else{
									right_node.filter(function(element){
										if(element.value !== value && value > center){
											right_node.push(new_node);
										}
									});
								}
							}
							return this;
						}else{
							current = current.left;
						}
					}else{
						if(! current.right){
							current.right = new_node;
							if(value < center){
								if(left_node.length == 0 && value < center){
									left_node.push(new_node);
								}else{
									left_node.filter(function(element){
										if(element.value !== value && value < center){
											left_node.push(new_node);
										}
									});
								}
							}else{
								if(right_node.length == 0 && value > center){
									right_node.push(new_node);
								}else{
									right_node.filter(function(element){
										if(element.value !== value && value > center){
											right_node.push(new_node);
										}
									});
								}
							}
							return this;
						}else{
							current = current.right;
						}
					}
				}
			}
		}
	}
	class Node{
		constructor(up, value, names, pkt){
			this.value = value;
	        this.left = 0;
	        this.right = 0;
	        this.up = up;
	        this.name = names;
	        this.paket = pkt;
		}
	}
	//getting from database
	swal({
        showConfirmButton   : false,
        allowOutsideClick   : false,
        allowEscapeKey      : false,
        background          : 'transparent',
        onOpen  : function(){
        	swal.showLoading();
        	setTimeout(function(){
        		all_tree();
        	},500);
        }
    });
	function all_tree(){
		tree = new BST();
		list_node = [];
		tree_node = [];
		left_node = [];
		right_node = [];
		$.ajax({
            type           : 'ajax',
            method         : 'get',
            url            : site + 'diagram/all_tree',
            async          : true,
            dataType       : 'json',
            success        : function(data){
            	if(data == false){
            		swal({
                        html        : '<pre>Node belum tersedia</pre>',
                        type        : "warning",
                        background  : 'transparent'
                    });
            	}else{
            		for(i = 0; i < data.length; i ++){
            			list_node.push(data[i].node);
            			tree.insert(data[i].up, data[i].node, data[i].name, data[i].paket);
            			left_node = left_node.filter((thing, index, self) =>
						  	index === self.findIndex((t) => (
						    	t.value === thing.value
						  	))
						)
						right_node = right_node.filter((thing, index, self) =>
						  	index === self.findIndex((t) => (
						    	t.value === thing.value
						  	))
						)
            		}
            		tree_node = data;
            		all_draw();
            	}
            },
            error     : function(){
                swal({
                    html        : '<pre>Koneksi terputus' + '<br>' +
                                  'Cobalah beberapa saat lagi</pre>',
                    type        : "warning",
                    background  : 'transparent'
                });
            }
        });
	}
	//draw into html view
	function all_draw(){
		if(tree_node[0].node != ''){
			tree_node.unshift({
				node 	: '',
				down 	: [tree_node[0].node],
				up 		: tree_node[0].up,
				name 	: tree_node[0].name,
				paket 	: tree_node[0].paket
			});
		}
		for(i = 0; i < tree_node.length; i ++){
			if(i > 1 && tree_node[i].up == 0){
				var tree_data = tree_node.filter(function(element){
					return element.down.includes(tree_node[i].node);
				});
				if(tree_data.length == 1){
					if(typeof tree_data[0].paket.split('_')[2] == 'undefined'){
						swal({
							html        : '<pre>Gagal menambahkan member' + '<br>' + 
										  'Upline belum tersedia</pre>',
		                    background  : 'transparent',
		                    type        : 'warning'
		                }).then(function(){
		                	swal({
						        showConfirmButton   : false,
						        allowOutsideClick   : false,
						        allowEscapeKey      : false,
						        background          : 'transparent',
						        onOpen  : function(){
						        	swal.showLoading();
						        	$('.tf-tree').empty();
						        	setTimeout(function(){
						        		all_tree();
						        	},500);
						        }
						    });
		                });
		                break;
					}else{
						tree_node[i].up = Number(tree_data[0].paket.split('_')[2]);
					}
				}
			}
			var level = '', node = '', start = '', end = '';
			var tree_paket = '';
			if(tree_node[i].paket != 0 && tree_node[i].paket != '_'){
				tree_paket = '<img src="' + site + 'assets/antahassy/icon/icons/' + tree_node[i].paket.split('_')[1] + '" width="15" height="15" style="margin-top: -5px;">';
			}
			// Math.min(...list_node)
			if(tree_node[i].node == ''){
				level = 
				'<ul>' +
			    	'<li id="lvl_' + tree_node[i].down[0] + '">' +
			      		'<span class="tf-nc">' + tree_node[i].down[0] + '<br><b style="margin-right: 5px;">' + tree_node[i].name + '</b><br>' + tree_paket + '</span>' +
			    	'</li>' +
			  	'</ul>';
			  	$('.tf-tree').html(level);
			}else{
				node = '<span class="tf-nc">' + tree_node[i].node + '<br><b style="margin-right: 5px;">' + tree_node[i].name + '</b><br>' + tree_paket + '</span>';
				start = '<ul>';
				end = '';
				for(x = 0; x < tree_node[i].down.length; x ++){
					if(tree_node[i].down[x] != 0){
						end +=
						'<li id="lvl_' + tree_node[i].down[x] + '">' +
				      		'<span class="tf-nc">' + tree_node[i].down[x] + '</span>' +
				    	'</li>';
					}
				}
				end += '</ul>';
				$('#lvl_' + tree_node[i].node).html(node + start + end);
			}
			if(i == (tree_node.length - 1)){
				swal.close();
				$('#back_content').hide();
			}
		}
		if(tree_node.length >= 4){
			$('#process_content').show();
		}
		//console.log(treeify.asTree(tree, true));
		//console.log(tree.root);
	}
	//array unique
	function unique(list) {
	    var result = [];
	    $.each(list, function(i, e) {
	        if($.inArray(e, result) == -1){
	            result.push(e);
	        }
	    });
	    return result;
	}
	main();
	function main(){
		var modal_tree_data;
        $('#modal_tree_data').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_tree_data = true;
        });
        $('#modal_tree_data').on('hide.bs.modal', function(){
            if(modal_tree_data){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_tree_data = false;
                setTimeout(function(){
                    $('#modal_tree_data').modal('hide');
                },350);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        $('#data_node').on('keyup', function(){
			var value 	= $(this).val();
		    var int = /^[0-9]+$/gm;
		    if(! int.test(value)) {
		        $(this).val('');
		    }
		});
		$('#btn_add_tree_data').on('click', function(){
			var errormessage    = '';
            if(! $('#data_node').val()){
                 errormessage += 'Node dibutuhkan \n';
            }
            if(! $('#data_name').val()){
                 errormessage += 'Nama dibutuhkan \n';
            }
            if($('#data_paket').val() == '0'){
                 errormessage += 'Paket dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
            	var node_up = Number($('#data_upline').val());
            	var node_val = Number($('#data_node').val());
            	var node_nm = $('#data_name').val();
            	var node_paket = $('#data_paket').val();
				swal({
		            showConfirmButton   : false,
		            allowOutsideClick   : false,
		            allowEscapeKey      : false,
		            background          : 'transparent',
		            onOpen  : function(){
		                swal.showLoading();
		                setTimeout(function(){
		                	$.ajax({
                                type           : 'ajax',
                                method         : 'post',
                                url            : site + 'diagram/check_node',
                                data           : {
                                     node    : node_val
                                },
                                async          : true,
                                dataType       : 'json',
                                success        : function(response){
                                    if(response.exist){
                                    	swal({
											html        : '<pre>Node sudah ada' + '<br>' + 
														  'Harap masukkan node lain</pre>',
						                    background  : 'transparent',
						                    type        : 'warning'
						                });
                                    }else{
										$.ajax({
			                                type           : 'ajax',
			                                method         : 'post',
			                                url            : site + 'diagram/check_account',
			                                data           : {
			                                     username  : node_nm
			                                },
			                                async          : true,
			                                dataType       : 'json',
			                                success        : function(response){
			                                    if(response.exist){
			                                    	swal({
														html        : '<pre>Username sudah ada' + '<br>' + 
																	  'Harap masukkan username lain</pre>',
									                    background  : 'transparent',
									                    type        : 'warning'
									                });
			                                    }else{
			                                    	$('#modal_tree_data').modal('hide');
													tree.insert(node_up, node_val, node_nm, node_paket);
													left_node = left_node.filter((thing, index, self) =>
													  	index === self.findIndex((t) => (
													    	t.value === thing.value
													  	))
													)
													right_node = right_node.filter((thing, index, self) =>
													  	index === self.findIndex((t) => (
													    	t.value === thing.value
													  	))
													)
													tree_node = [tree_node[0]];
													if(tree_node.length == 1){
														tree_node.push({
															node	: tree_node[0].down[0],
															up		: tree_node[0].up,
															down 	: [left_node[0].value, right_node[0].value],
															name 	: tree_node[0].name,
															paket 	: tree_node[0].paket
														});
													}
													for(i = 1; i < tree_node.length; i ++){
														for(x = 0; x < tree_node[i].down.length; x ++){
															if(tree_node[i].down[x] != 0){
																var left_hand = left_node.filter(function(element){
																	return element.value === tree_node[i].down[x];
																});
																var right_hand = right_node.filter(function(element){
																	return element.value === tree_node[i].down[x];
																});
																if(typeof left_hand[0] != 'undefined'){
																	var kiri = 0;
																	if(left_hand[0].left != 0){
																		kiri = left_hand[0].left.value
																	}
																	var kanan = 0;
																	if(left_hand[0].right != 0){
																		kanan = left_hand[0].right.value
																	}
																	tree_node.push({
																		node 	: tree_node[i].down[x],
																		up 		: left_hand[0].up,
																		down 	: [kiri, kanan],
																		name	: left_hand[0].name,
																		paket	: left_hand[0].paket
																	});
																}else{
																	var kiri = 0;
																	if(right_hand[0].left != 0){
																		kiri = right_hand[0].left.value
																	}
																	var kanan = 0;
																	if(right_hand[0].right != 0){
																		kanan = right_hand[0].right.value
																	}
																	tree_node.push({
																		node 	: tree_node[i].down[x],
																		up 		: right_hand[0].up,
																		down 	: [kiri, kanan],
																		name	: right_hand[0].name,
																		paket	: right_hand[0].paket
																	});
																}
															}
														}
													}
													all_draw();
			                                    }
			                                },
			                                error     : function(){
			                                    swal({
			                                        html        : '<pre>Koneksi terputus' + '<br>' +
			                                                      'Cobalah beberapa saat lagi</pre>',
			                                        type        : "warning",
			                                        background  : 'transparent'
			                                    });
			                                }
			                            });
									}
                                },
                                error     : function(){
                                    swal({
                                        html        : '<pre>Koneksi terputus' + '<br>' +
                                                      'Cobalah beberapa saat lagi</pre>',
                                        type        : "warning",
                                        background  : 'transparent'
                                    });
                                }
                            });
				    	},500);
		            }
		        });
			}
		});
		$('#btn_node_find').on('click', function(){
			var node_val = $('#node_value').val();
			if(node_val == ''){
				swal({
					html        : '<pre>Node / Nama dibutuhkan</pre>',
                    background  : 'transparent',
                    type        : 'warning'
                });
			}else{
				$('.tf-tree li').css('background','transparent');
				if(! isNaN(node_val)){
					$('.tf-tree #lvl_' + node_val).css('background','yellow');
				}else{
					var tree_data = tree_node.filter(function(element){
						return element.name === node_val;
					});
					if(tree_data.length != 0){
						if(tree_data.length > 1){
							$('.tf-tree #lvl_' + tree_data[1].node).css('background','yellow');
						}else{
							$('.tf-tree #lvl_' + tree_data[0].node).css('background','yellow');
						}
					}
				}
			}
		});
		$('#btn_node_min').on('click', function(){
			list_node = unique(list_node);
			var tree_data = tree_node.filter(function(element){
				return element.node === Math.min(...list_node);
			});
			$('#node_min').html(tree_data[0].node + ' (' + tree_data[0].name + ')');
		});
		$('#btn_node_max').on('click', function(){
			list_node = unique(list_node);
			var tree_data = tree_node.filter(function(element){
				return element.node === Math.max(...list_node);
			});
			$('#node_max').html(tree_data[0].node + ' (' + tree_data[0].name + ')');
		});
		$('#btn_process').on('click', function(event){
			event.preventDefault();
			event.stopImmediatePropagation();
			if(tree_node.length < 4){
				swal({
                    html        : '<pre>Level kurang</pre>',
                    type        : "warning",
                    background  : 'transparent'
                });
			}else{
				swal({
	                html                : '<pre>Diagram lama akan terhapus' + '<br>' + 
	                					  'Simpan diagram saat ini ?</pre>',
	                type                : "question",
	                background          : 'transparent',
	                showCancelButton    : true,
	                cancelButtonText    : 'Tidak',
	                confirmButtonText   : 'Ya'
	            }).then((result) => {
	            	if(result.value){
	            		swal({
		                    showConfirmButton   : false,
		                    allowOutsideClick   : false,
		                    allowEscapeKey      : false,
		                    background          : 'transparent',
		                    onOpen  : function(){
		                        swal.showLoading();
		                        setTimeout(function(){
		                            $.ajax({
		                                type           : 'ajax',
		                                method         : 'post',
		                                url            : site + 'diagram/tree_process',
		                                data           : {
		                                     tree    : tree_node
		                                },
		                                async          : true,
		                                dataType       : 'json',
		                                success        : function(response){
		                                    if(response.success){
		                                    	$('.tf-tree').empty();
		                                    	setTimeout(function(){
		                                    		$.ajax({
							                            type           : 'ajax',
							                            method         : 'get',
							                            url            : site + 'process/pairing',
							                            async          : true,
							                            dataType       : 'json',
							                            success        : function(response){
							                                if(response.success){
							                                    setTimeout(function(){
							                                        all_tree();
							                                    },500);
							                                }
							                            },
							                            error     : function(){
							                                swal({
							                                    html        : '<pre>Koneksi terputus' + '<br>' +
							                                                  'Cobalah beberapa saat lagi</pre>',
							                                    type        : "warning",
							                                    background  : 'transparent'
							                                });
							                            }
							                        });
								            	},500);
		                                    }
		                                },
		                                error     : function(){
		                                    swal({
		                                        html        : '<pre>Koneksi terputus' + '<br>' +
		                                                      'Cobalah beberapa saat lagi</pre>',
		                                        type        : "warning",
		                                        background  : 'transparent'
		                                    });
		                                }
		                            });
		                        },500);
		                    }
		                });
	            	}
	            });
			}
		});
		$('#btn_main_tree').on('click', function(){
			swal({
	            showConfirmButton   : false,
	            allowOutsideClick   : false,
	            allowEscapeKey      : false,
	            background          : 'transparent',
	            onOpen  : function(){
	                swal.showLoading();
	                setTimeout(function(){
	                	all_draw();
			    	},500);
	            }
	        });
		});
		$('#btn_add').on('click', function(){
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                    	$.ajax({
                            type           : 'ajax',
                            method         : 'get',
                            url            : site + 'diagram/s_paket',
                            async          : true,
                            dataType       : 'json',
                            success        : function(data){
                            	$('#data_node').val('').css({'background':'#fff','color':'black'}).attr('readonly', false);
								$('#data_name').val('');
								$('.btn_modal_action').hide();
								$('#btn_add_tree_data').show();
								$('#modal_tree_data').find('.modal-title').text('Tambah');
                            	var s_paket = '<option value="0">Pilih Paket</option>';
                            	if(data.false){
                            		$('#data_paket').html(s_paket);
                            	}else{
                            		for(i = 0; i < data.length; i ++){
                            			s_paket += '<option value="' + data[i].id_paket + '">' + data[i].nama + ' (' + Number(data[i].nilai).toLocaleString('de') + ')</option>';
                            		}
                            		$('#data_paket').html(s_paket);
                            	}
                            	swal.close();
                            	$('#modal_tree_data').modal('show');
                            },
                            error     : function(){
                                swal({
                                    html        : '<pre>Koneksi terputus' + '<br>' +
                                                  'Cobalah beberapa saat lagi</pre>',
                                    type        : "warning",
                                    background  : 'transparent'
                                });
                            }
                        });
                    },500);
                }
            });
        });
		$('.tf-tree').on('contextmenu', '.tf-nc', function(){
			var node_val = Number($(this).parent().attr('id').split('_')[1]);
			if(tree_edit == true){
				var tree_data = tree_node.filter(function(element){
					return element.node === node_val;
				});
				$('#data_node').val(tree_data[0].node).css({'background':'#3445E5','color':'#fff'}).attr('readonly', true);
				$('#data_name').val(tree_data[0].name);
				swal({
		            showConfirmButton   : false,
		            allowOutsideClick   : false,
		            allowEscapeKey      : false,
		            background          : 'transparent',
		            onOpen  : function(){
		                swal.showLoading();
		                setTimeout(function(){
		                	$.ajax({
	                            type           : 'ajax',
	                            method         : 'get',
	                            url            : site + 'diagram/s_paket',
	                            async          : true,
	                            dataType       : 'json',
	                            success        : function(data){
	                            	$('.btn_modal_action').hide();
	                            	$('#btn_process_tree_data').show();
	                            	$('#modal_tree_data').find('.modal-title').text('Edit');
	                            	var s_paket = '<option value="0">Pilih Paket</option>';
	                            	if(data.false){
	                            		$('#data_paket').html(s_paket);
	                            	}else{
	                            		for(i = 0; i < data.length; i ++){
	                            			s_paket += '<option value="' + data[i].id_paket + '">' + data[i].nama + ' (' + Number(data[i].nilai).toLocaleString('de') + ')</option>';
	                            		}
	                            		$('#data_paket').html(s_paket);
	                            		if(tree_data[0].paket != '_' && tree_data[0].paket != ''){
	                            			$('#data_paket').val(tree_data[0].paket.split('_')[0]);
	                            		}
	                            	}
	                            	swal.close();
	                            	$('#modal_tree_data').modal('show');
	                            },
	                            error     : function(){
	                                swal({
	                                    html        : '<pre>Koneksi terputus' + '<br>' +
	                                                  'Cobalah beberapa saat lagi</pre>',
	                                    type        : "warning",
	                                    background  : 'transparent'
	                                });
	                            }
	                        });
				    	},500);
		            }
		        });
			}else{
				swal({
                    html        : '<pre>Akses ditolak</pre>',
                    type        : "error",
                    background  : 'transparent'
                });
			}
			return false;
		});
		$('#btn_process_tree_data').on('click', function(){
			var errormessage    = '';
            if(! $('#data_node').val()){
                 errormessage += 'Node dibutuhkan \n';
            }
            if(! $('#data_name').val()){
                 errormessage += 'Nama dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
            	swal({
	                html                : '<pre>Diagram lama akan terhapus' + '<br>' + 
	                					  'Simpan diagram saat ini ?</pre>',
	                type                : "question",
	                background          : 'transparent',
	                showCancelButton    : true,
	                cancelButtonText    : 'Tidak',
	                confirmButtonText   : 'Ya'
	            }).then((result) =>{
	            	if(result.value){
	            		swal({
				            showConfirmButton   : false,
				            allowOutsideClick   : false,
				            allowEscapeKey      : false,
				            background          : 'transparent',
				            onOpen  : function(){
				            	$('#modal_tree_data').modal('hide');
				                swal.showLoading();
				                setTimeout(function(){
				                	for(i = 0; i < tree_node.length; i ++){
				                		if(tree_node[i].node == Number($('#data_node').val())){
				                			if(i == 1){
				                				tree_node[0].name = $('#data_name').val();
				                				tree_node[0].paket = $('#data_paket').val();
				                				tree_node[i].name = $('#data_name').val();
				                				tree_node[i].paket = $('#data_paket').val();
				                			}else{
				                				tree_node[i].name = $('#data_name').val();
				                				tree_node[i].paket = $('#data_paket').val();
				                			}
				                			break;
				                		}
				                	}
				                	$.ajax({
		                                type           : 'ajax',
		                                method         : 'post',
		                                url            : site + 'diagram/tree_process',
		                                data           : {
		                                     tree    : tree_node
		                                },
		                                async          : true,
		                                dataType       : 'json',
		                                success        : function(response){
		                                    if(response.success){
		                                    	$('.tf-tree').empty();
		                                    	setTimeout(function(){
								            		all_tree();
								            	},500);
		                                    }
		                                },
		                                error     : function(){
		                                    swal({
		                                        html        : '<pre>Koneksi terputus' + '<br>' +
		                                                      'Cobalah beberapa saat lagi</pre>',
		                                        type        : "warning",
		                                        background  : 'transparent'
		                                    });
		                                }
		                            });
						    	},500);
				            }
				        });
	            	}
	            });
            }
		});
		$('.tf-tree').on('click', '.tf-nc', function(){
			var node_val = Number($(this).parent().attr('id').split('_')[1]);
			var clicked = $(this);
			swal({
	            showConfirmButton   : false,
	            allowOutsideClick   : false,
	            allowEscapeKey      : false,
	            background          : 'transparent',
	            onOpen  : function(){
	                swal.showLoading();
	                setTimeout(function(){
	                	if(node_val != 0){
	                		var limit = 0;
							for(i = 0; i < tree_node.length; i ++){
								if(tree_node[i].node == node_val){
									limit = i;
									break;
								}
							}
							downline_node = [];
							for(i = 0; i < tree_node.length; i ++){
								if(i >= limit){
									downline_node.push(tree_node[i]);
								}
							}
							downline_node.unshift({
								node 		: '',
								down 	: [downline_node[0].node]
							});
					        for(i = 0; i < downline_node.length; i ++){
								var level = '', node = '', start = '', end = '';
								if(downline_node[i].node == ''){
									level = 
									'<ul>' +
								    	'<li id="lvl_' + downline_node[i].down[0] + '">' +
								      		'<span class="tf-nc">' + downline_node[i].down[0] + '</span>' +
								    	'</li>' +
								  	'</ul>';
								  	$('.tf-tree').html(level);
								}else{
									node = '<span class="tf-nc">' + downline_node[i].node + '</span>';
									start = '<ul>';
									end = '';
									for(x = 0; x < downline_node[i].down.length; x ++){
										end +=
										'<li id="lvl_' + downline_node[i].down[x] + '">' +
								      		'<span class="tf-nc">' + downline_node[i].down[x] + '</span>' +
								    	'</li>';
									}
									end += '</ul>';
									$('#lvl_' + downline_node[i].node).html(node + start + end);
								}
								if(i == (downline_node.length - 1)){
									swal.close();
									$('#back_content').show();
								}
							}
	                	}else{
	                		var node = '<span class="tf-nc">' + node_val + '</span>';
							var start = '<ul>';
							var end = '';
							for(x = 0; x < 2; x ++){
								end +=
								'<li id="lvl_' + 0 + '">' +
						      		'<span class="tf-nc">' + 0 + '</span>' +
						    	'</li>';
							}
							end += '</ul>';
	                		clicked.parent().html(node + start + end);
	                		swal.close();
	                	}
			    	},500);
	            }
	        });
		});
	}
});