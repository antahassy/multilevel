-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 23, 2021 at 05:44 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_multilevel`
--

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `deleted_by` varchar(50) NOT NULL,
  `created_at` varchar(50) NOT NULL,
  `updated_at` varchar(50) NOT NULL,
  `deleted_at` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', 'Administrator', 'System', 'administrator', '', '2021-03-23 04:22:20', '2021-03-30 10:45:00', ''),
(2, 'member', 'Member', 'administrator', 'administrator', '', '2021-09-13 14:43:17', '2021-09-13 14:48:36', '');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id_menu` int(11) NOT NULL,
  `nama_menu` varchar(150) NOT NULL,
  `url` varchar(500) DEFAULT NULL,
  `rel` int(11) NOT NULL,
  `rel2` int(11) NOT NULL,
  `is_trash` int(11) NOT NULL,
  `urutan` int(11) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `created_at` varchar(50) NOT NULL,
  `updated_at` varchar(50) NOT NULL,
  `deleted_by` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id_menu`, `nama_menu`, `url`, `rel`, `rel2`, `is_trash`, `urutan`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_by`) VALUES
(1, 'Dashboard', 'dashboard', 0, 0, 0, 1, 'System', 'administrator', '2021-04-01 12:38:31', '2021-05-10 10:01:49', ''),
(2, 'Paket', 'paket', 0, 0, 0, 2, 'administrator', 'administrator', '2021-09-13 14:44:58', '2021-09-13 15:09:15', ''),
(3, 'Diagram', 'diagram', 0, 0, 0, 3, 'administrator', 'administrator', '2021-09-14 13:08:39', '2021-09-14 13:08:44', ''),
(4, 'Bonus', 'master', 0, 0, 0, 4, 'administrator', '', '2021-09-15 12:32:28', '', ''),
(5, 'Bonus Sponsor', 'bonus_sponsor', 4, 0, 0, 1, 'administrator', '', '2021-09-15 12:32:47', '', ''),
(6, 'Bonus Pairing', 'bonus_pairing', 4, 0, 0, 2, 'administrator', '', '2021-09-15 12:33:02', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `rel_group`
--

CREATE TABLE `rel_group` (
  `id_group` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `akses` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `rel_group`
--

INSERT INTO `rel_group` (`id_group`, `id_menu`, `akses`) VALUES
(2, 6, 3),
(2, 5, 3),
(2, 4, 3),
(2, 3, 3),
(2, 1, 3),
(1, 6, 3),
(1, 5, 3),
(1, 4, 3),
(1, 6, 1),
(1, 3, 3),
(2, 3, 1),
(1, 2, 3),
(1, 2, 1),
(1, 2, 4),
(1, 1, 3),
(1, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id_setting` int(11) NOT NULL,
  `meta_id` varchar(50) DEFAULT NULL,
  `value` varchar(350) DEFAULT NULL,
  `updated_by` varchar(50) NOT NULL,
  `updated_at` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id_setting`, `meta_id`, `value`, `updated_by`, `updated_at`) VALUES
(1, 'judul_web', 'Tree Multilevel', 'administrator', '2021-09-13 14:54:09'),
(2, 'deskripsi', 'Tree Multilevel', 'administrator', '2021-09-13 14:54:09'),
(3, 'logo', 'icon.png', 'administrator', '2021-09-13 14:54:09'),
(4, 'judul_navbar', 'Tree Multilevel', 'administrator', '2021-09-13 14:54:09');

-- --------------------------------------------------------

--
-- Table structure for table `tb_bonus_pairing`
--

CREATE TABLE `tb_bonus_pairing` (
  `id_bonus_pairing` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `tanggal` varchar(50) NOT NULL,
  `id_kiri` bigint(20) NOT NULL,
  `kiri` bigint(20) NOT NULL,
  `sisa_kiri` bigint(20) NOT NULL,
  `id_kanan` bigint(20) NOT NULL,
  `kanan` bigint(20) NOT NULL,
  `sisa_kanan` bigint(20) NOT NULL,
  `pairing` bigint(20) NOT NULL,
  `bonus` bigint(20) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `deleted_by` varchar(50) NOT NULL,
  `created_at` varchar(50) NOT NULL,
  `updated_at` varchar(50) NOT NULL,
  `deleted_at` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_bonus_sponsor`
--

CREATE TABLE `tb_bonus_sponsor` (
  `id_bonus_sponsor` bigint(20) NOT NULL,
  `sponsor_id` bigint(20) NOT NULL,
  `tanggal` varchar(50) NOT NULL,
  `bonus` bigint(20) NOT NULL,
  `upline_id` bigint(20) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  `paket` varchar(100) NOT NULL,
  `nilai` bigint(20) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `deleted_by` varchar(50) NOT NULL,
  `created_at` varchar(50) NOT NULL,
  `updated_at` varchar(50) NOT NULL,
  `deleted_at` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_bonus_sponsor`
--

INSERT INTO `tb_bonus_sponsor` (`id_bonus_sponsor`, `sponsor_id`, `tanggal`, `bonus`, `upline_id`, `member_id`, `paket`, `nilai`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, '2021-09-14', 20, 2, 3, 'Paket 2', 200, 'member1', '', '', '2021-09-14 16:24:40', '', ''),
(2, 2, '2021-09-14', 30, 2, 4, 'Paket 3', 300, 'member1', '', '', '2021-09-14 16:24:57', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_member`
--

CREATE TABLE `tb_member` (
  `id_member` bigint(20) NOT NULL,
  `id_upline` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `id_paket` tinyint(4) NOT NULL,
  `node` bigint(20) NOT NULL,
  `downline` tinytext NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `deleted_by` varchar(50) NOT NULL,
  `created_at` varchar(50) NOT NULL,
  `updated_at` varchar(50) NOT NULL,
  `deleted_at` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_member`
--

INSERT INTO `tb_member` (`id_member`, `id_upline`, `id_user`, `id_paket`, `node`, `downline`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, 2, 1, 100, '[75,125]', 'administrator', 'member1', '', '2021-03-23 04:22:20', '2021-09-23 10:36:09', ''),
(2, 2, 3, 2, 75, '[0,0]', 'member1', 'member1', '', '2021-03-23 04:22:20', '2021-09-23 10:36:09', ''),
(3, 2, 4, 3, 125, '[0,0]', 'member1', 'member1', '', '2021-03-23 04:22:20', '2021-09-23 10:36:09', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_paket`
--

CREATE TABLE `tb_paket` (
  `id_paket` bigint(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nilai` bigint(20) NOT NULL,
  `icon` varchar(250) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `deleted_by` varchar(50) NOT NULL,
  `created_at` varchar(50) NOT NULL,
  `updated_at` varchar(50) NOT NULL,
  `deleted_at` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_paket`
--

INSERT INTO `tb_paket` (`id_paket`, `nama`, `nilai`, `icon`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Paket 1', 100, 'alarm.svg', 'administrator', 'administrator', '', '2021-09-03 11:12:38', '2021-09-13 15:48:03', ''),
(2, 'Paket 2', 200, 'archive.svg', 'administrator', 'administrator', '', '2021-09-03 11:12:57', '2021-09-13 15:47:57', ''),
(3, 'Paket 3', 300, 'arrow-down-up.svg', 'administrator', 'administrator', '', '2021-09-03 11:13:15', '2021-09-13 15:47:48', ''),
(4, 'Paket 4', 400, 'arrows-fullscreen.svg', 'administrator', 'administrator', '', '2021-09-04 12:00:45', '2021-09-13 15:47:42', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_value`
--

CREATE TABLE `tb_value` (
  `id_value` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `id_upline` bigint(20) NOT NULL,
  `tanggal` varchar(50) NOT NULL,
  `value` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(250) NOT NULL,
  `logged_in` varchar(50) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` text NOT NULL,
  `active` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `image` longtext NOT NULL,
  `nama` varchar(100) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `deleted_by` varchar(50) NOT NULL,
  `created_at` varchar(50) NOT NULL,
  `updated_at` varchar(50) NOT NULL,
  `deleted_at` varchar(50) NOT NULL,
  `remember_selector` varchar(250) NOT NULL,
  `remember_code` varchar(250) NOT NULL,
  `forgotten_password_selector` varchar(250) NOT NULL,
  `forgotten_password_code` varchar(250) NOT NULL,
  `forgotten_password_time` int(11) NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `logged_in`, `username`, `password`, `active`, `email`, `phone`, `image`, `nama`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`, `remember_selector`, `remember_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `last_login`) VALUES
(1, '180.254.67.20', '2021-09-23 10:29:53', 'administrator', '$2y$12$rUSpsAsJgzDYHK4mCodPlegFDhykWay8hw3oSoaw..w2arWi1u32i', 1, '', '', '', 'Administrator', 'System', '', '', '2021-03-23 04:22:20', '', '', '', '', '', '', 0, 1632367793),
(2, '180.254.67.20', '2021-09-23 10:30:09', 'member1', '$2y$10$m.7Ue27KEDJDhWvw7NBvmOxTyBOfCbAzda2V1aZWuT9xT.Pb6q2SK', 1, '', '', '', 'member1', 'administrator', '', '', '2021-09-13 14:51:31', '', '', '', '', '', '', 0, 1632367809),
(3, '180.254.73.66', '2021-09-17 15:44:08', 'member2', '$2y$10$WC.HqXheepfZ1Gg9TDgh0Oof7h8GOKRI2PrFjCpQNf4Xqc8SmOKSi', 1, '', '', '', 'member2', 'member1', '', '', '2021-09-14 16:24:40', '', '', '', '', '', '', 0, 1631868248),
(4, '180.254.66.164', '2021-09-17 14:07:42', 'member3', '$2y$10$Xop6cbsfFkydHIehlnx8/.R7cX73xSDsVvJVXlv4gVtVpFnXcDn3K', 1, '', '', '', 'member3', 'member1', '', '', '2021-09-14 16:24:57', '', '', '', '', '', '', 0, 1631862462);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 2),
(4, 4, 2),
(5, 5, 2),
(6, 6, 2),
(7, 7, 2),
(8, 8, 2),
(9, 9, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id_setting`);

--
-- Indexes for table `tb_bonus_pairing`
--
ALTER TABLE `tb_bonus_pairing`
  ADD PRIMARY KEY (`id_bonus_pairing`);

--
-- Indexes for table `tb_bonus_sponsor`
--
ALTER TABLE `tb_bonus_sponsor`
  ADD PRIMARY KEY (`id_bonus_sponsor`);

--
-- Indexes for table `tb_member`
--
ALTER TABLE `tb_member`
  ADD PRIMARY KEY (`id_member`);

--
-- Indexes for table `tb_paket`
--
ALTER TABLE `tb_paket`
  ADD PRIMARY KEY (`id_paket`);

--
-- Indexes for table `tb_value`
--
ALTER TABLE `tb_value`
  ADD PRIMARY KEY (`id_value`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id_setting` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_bonus_pairing`
--
ALTER TABLE `tb_bonus_pairing`
  MODIFY `id_bonus_pairing` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_bonus_sponsor`
--
ALTER TABLE `tb_bonus_sponsor`
  MODIFY `id_bonus_sponsor` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_member`
--
ALTER TABLE `tb_member`
  MODIFY `id_member` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_paket`
--
ALTER TABLE `tb_paket`
  MODIFY `id_paket` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_value`
--
ALTER TABLE `tb_value`
  MODIFY `id_value` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
